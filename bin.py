"""
# correct the areas
cows = data["cowid"].drop_duplicates().reset_index(drop=1)
for cow in cows:
    print(cow)
    days = data.loc[(data["cowid"]==cow),["date"]].drop_duplicates() 
    days = days.sort_values(by = ["date"]).reset_index(drop=1)
    for dd in days["date"]:
        print(dd)
        barnnr = int(data.loc[(data["cowid"] == cow) &
                          (data["date"] == dd),"barn"].mean()) 
        myarea = area_zones.loc[(area_zones["barn"] == barnnr) | \
                            (area_zones["barn"].isna() == True) \
                            ,:].reset_index(drop=1)
        for j in range(0,len(myarea)):
            x1,y1,x2,y2,x3,y3,x4,y4 = myarea[["x1","y1","x2","y2","x3","y3","x4","y4"]].values[j]
            data.loc[((data["cowid"] == cow) & (data["date"] == dd) & \
                      (data["xnew"] >= x1) & (data["xnew"] <= x2) & \
                      (data["xnew"] >= x3) & (data["xnew"] <= x4) & \
                      (data["ynew"] >= y1) & (data["ynew"] >= y2) & \
                      (data["ynew"] <= y3) & (data["ynew"] <= y4)),"area"] = myarea["area"][j]
            data.loc[((data["cowid"] == cow) & (data["date"] == dd) & \
                      (data["xnew"] >= x1) & (data["xnew"] <= x2) & \
                      (data["xnew"] >= x3) & (data["xnew"] <= x4) & \
                      (data["ynew"] >= y1) & (data["ynew"] >= y2) & \
                      (data["ynew"] <= y3) & (data["ynew"] <= y4)),"zone"] = myarea["zone"][j]
        del x1,y1,x2,y2,x3,y3,x4,y4
del cows, dd, days
"""