# -*- coding: utf-8 -*-
"""
Created on Mon Feb  6 11:20:43 2023

@author: adria036
"""
#%% import packages

import pandas as pd
# from datetime import  date,timedelta
import numpy as np
# from read_data_sewio import start_end_dates, read_all_data, read_ids, combine_data_id
import os
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt


# path_out 
path_out = r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\nlas\results\exploration"


#%% visualisation functions

def heatmap_barn(data_x,data_y,xstep,ystep,x_lim,y_lim):
    """
    Plot the frequency of positions registered, given data_x and data_y
    xstep and ystep determine how fine these positions are taken
    x_lim and y_lim set boundaries (e.g. incl feeding lane)
    
    Parameters
    ----------
    data_x : TYPE = Pandas series
        Data of the x-coordinate as registered by the sewio system
    data_y : TYPE = Pandas series
        Data of the y-coordinate as registered by the sewio system
    xstep : TYPE = float
        How fine you want to determine location in x-direction (e.g. 1 is one
            meter, 0.1 = 10 centimeters)
    ystep : TYPE = float
        How fine you want to determine location in x-direction (e.g. 1 is one
            meter, 0.1 = 10 centimeters)
    x_lim : TYPE = list of floats
        Physical boundaries of plot you want to make (e.g. cut off feeding lane
            then indicate lower boundary in y direction = -4)
    y_lim : TYPE = list of floats
        Physical boundaries of plot you want to make (e.g. cut off feeding lane
            then indicate lower boundary in y direction = -4)

    Returns
    -------
    ax : TYPE = sns heatmap
        figure axes with heatmap

    """
    # transform data_x and data_y for count
    x_transf = (data_x*(1/xstep)).round()*xstep
    y_transf = (data_y*(1/ystep)).round()*ystep
    
    # put x_transf and y_transf in DataFrame
    frame = {'xT' : x_transf.round(decimals = 4),
             'yT' : y_transf.round(decimals = 4) }
    df = pd.DataFrame(frame)
    
    # count and groupby
    heatdata = df.groupby(["xT","yT"]).size()
    heatdata = heatdata.to_frame(name = "counts")
    heatdata = heatdata.reset_index()
    
    # set limits to agree with steps
    x_low = round(round(x_lim[0]*(1/xstep))*xstep,ndigits = 4)
    x_high = round(round(x_lim[1]*(1/xstep))*xstep,ndigits = 4)
    y_low = round(round(y_lim[0]*(1/ystep))*ystep,ndigits = 4)
    y_high = round(round(y_lim[1]*(1/ystep))*ystep,ndigits = 4)
    
    # delete the rows for which values are outside x_lim or y_lim
    heatdata = heatdata.loc[(heatdata["xT"] >= x_lim[0]) &
                 (heatdata["xT"] <= x_lim[1]) &
                 (heatdata["yT"] >= y_lim[0]) &
                 (heatdata["yT"] <= y_lim[1])]
    heatdata.reset_index(drop = True, inplace = True)
    
    # expected axes limits based on lim and step
    x = np.arange(x_low,x_high+xstep,xstep,dtype = float)
    y = np.arange(y_low,y_high+ystep,ystep,dtype = float)
    
    # fill array
    heat_all = []
    for i in y:
        for ii in x:
            number_values = heatdata.loc[(heatdata["xT"] == round(ii,ndigits = 4)) &
                                         (heatdata["yT"] == round(i,ndigits = 4)),
                                         ["counts"]]
            if number_values.empty:
                number_values = pd.DataFrame(data = [0], columns = ["counts"])
            number_values = number_values.reset_index(drop=1)
            # print(round(i,ndigits = 4),round(ii,ndigits = 4),number_values["counts"][0])
            heat_all.append(number_values["counts"][0])
            
        
    # make rectangular and convert list to numpy
    heat_all = np.array(heat_all)
    heat_all = np.reshape(heat_all, [len(y),len(x)])
    heat_all = heat_all.transpose()
    test = np.flip(heat_all, axis = 1)
    
    
    # convert to df with right colnames and indices
    xcols = np.array(x.round(decimals=3),dtype = str)
    ycols = np.flip(np.array(y.round(decimals=3),dtype = str))
    df = pd.DataFrame(test, columns = ycols, index = xcols)
    
    # make heatmap
    ax = sns.heatmap(df,
                     yticklabels = round(1/xstep),
                     xticklabels = round(1/ystep*2),
                     robust = True,
                     cbar = False,
                     cmap = "Greys_r"
                     # alpha = 0.5
                     )

    return ax


#%% bout calculations
def behaviour_bouts(data, interval, behaviour,min_length):
    """
    Parameters
    ----------
    data : Pandas DataFrame
        data array with at least: 
            - t           : time stamp
            - xnew, ynew  : preprocessed position
            - area        : area (walking, drink_trough, cubicle_A-B-C, feed_rack) 
            - zone
    interval : numeric
        time in seconds that the behaviour is discontinued to belong to another
        bout

    Returns
    -------
    df : Pandas DataFrame
        dataframe with bouts of selected behaviour, according to max interval 
        of interval between two bouts
    summary : Pandas DataFrame
        summary of the behaviour for that day

    """
    
    #---------------------------- for development -----------------------------
    # interval = 5*60
    # behaviour = "unknown"
    # min_length = 5
    # sep = {"cubicle": 60, "walking" : 3*60, "drink_trough" : 5*60}
    #---------------------------- for development -----------------------------
    
    # select data of behaviour of interest and calculate discontinuities
    subset = data.loc[(data["area"].str.contains(behaviour)),:].copy().reset_index(drop=1)
    if len(subset) > 0:
        subset["gap"] = subset["t"].diff()
        subset["gapprev"] = np.nan
        subset.iloc[0:-1,subset.columns == "gapprev"] = subset.iloc[1:,subset.columns == "gap"]
        
        # keep where gap > interval and add previous without gap to mark end of bout
        idx = np.array(subset.loc[subset["gap"] > interval].index.values)
        idxstart = np.sort(np.append(idx,subset.index.values[0]))
        idx = np.array(subset.loc[subset["gapprev"] > interval].index.values)
        idxend = np.sort(np.append(idx,subset.index.values[-1]))
    
        df = subset.loc[idxstart,:].reset_index(drop=1)
        df["end_time"] = subset.loc[idxend,"t"].values
        
        # add time to check with figures
        df["bhour"] = np.floor(df["t"]/3600)
        df["bmin"] = np.floor((df["t"]-df["bhour"]*3600)/60)
        df["ehour"] = np.floor(df["end_time"]/3600)
        df["emin"] = np.floor((df["end_time"]-df["ehour"]*3600)/60)
            
        # for the bout length, calculate both true number of times behaviour
        #    is recorded, and end - begin incl the other behaviours in between
        #    => if "unknown", also put it as the behaviour of interest
        df["len1"] = round(((df["end_time"] - df["t"])+1)/60,2)
        
        # calculate true number of seconds this behaviour is noted
        df["len2"] = np.nan
        for i in range(0,len(df)):
            df.loc[i,"len2"] = round(len(data.loc[(data["t"] >= df["t"][i]) & \
                                                  (data["t"] <= df["end_time"][i]) & \
                                                  ((data["area"].str.contains(behaviour)) | \
                                                   (data["area"].str.contains("unknown")))])/60,2)
        
        # only keep bouts > min_length
        df = df.loc[df["len1"]*60 > min_length,:].reset_index(drop=1)
        
        if len(df) > 0:
            # calculate gap between successive bouts
            df["gap"] = np.nan
            df.iloc[1:,df.columns == "gap"] = np.round((df["t"][1:].values - df["end_time"][0:-1].values)/60)
        
            # add behaviour and select data
            df["behaviour"] = behaviour
            df["btime"] = pd.to_datetime(df["date"]) + pd.to_timedelta(df["t"],unit = 's')
            df["etime"] = pd.to_datetime(df["date"]) + pd.to_timedelta(df["end_time"],unit = 's')
            df = df[["cowid","barn","behaviour","date","t","end_time","btime","etime",
                     "gap","len1","len2"]]
            df = df.rename(columns = {"end_time" : "end","t" : "start"})
                
            # calculate summary 
            summary = df[["cowid","barn","date","behaviour","len1","len2"]].groupby(by = ["cowid","barn","date","behaviour"]).mean().round(1).reset_index()
            summary = summary.rename(columns = {"len1" : "len1_av", "len2" : "len2_av"})
            summary["no_bouts"] = len(df)
            summary["gap_med"] = df["gap"].median()
            summary["total1"] = df["len1"].sum()
            summary["total2"] = df["len2"].sum()
            summary["nmeas"] = len(data.loc[data["area"].str.contains(behaviour)])
            summary["perc"] = round(len(data.loc[data["area"].str.contains(behaviour)]) / \
                              len(data) * 100,2)
            #print(summary)
        else:
            df = pd.DataFrame([],columns = ["cowid","barn","behaviour","date","start",
                                            "end","btime","etime",
                                            "gap","len1","len2"])
            summary = pd.DataFrame([],columns = ["cowid","barn","date","behaviour",
                                                 "len1_av","len2_av","no_bouts",
                                                 "gap_med","total1","total2"])
        
    else:
        df = pd.DataFrame([],columns = ["cowid","barn","behaviour","date","start",
                                        "end","btime","etime",
                                        "gap","len1","len2"])
        summary = pd.DataFrame([],columns = ["cowid","barn","date","behaviour",
                                             "len1_av","len2_av","no_bouts",
                                             "gap_med","total1","total2"])
    
    return df,summary


"""
#%% usage
# ------------------------- individual cow heatmaps ---------------------------
# unique cows
cows = data.loc[:,["cowid"]].drop_duplicates() 
cows = cows.sort_values(by = ["cowid"]).reset_index(drop=1)



# settings heatmap
xstep = 0.1
ystep = 0.1

#barn limits
barn = {"60":[64.8,75.6],
        "61":[54,64.8],
        "62":[32.4,54],
        "70":[21.6,32.4],
        "71":[10.8,21.6],
        "72":[0,10.8],
        "73":[-10.8,0],
        }

# plots per cow-day
for cow in cows["cowid"]:
    
    # unique days
    days = data.loc[(data["cowid"]==cow),["date"]].drop_duplicates() 
    days = days.sort_values(by = ["date"]).reset_index(drop=1)
    
    # select data and create heatmaps
    for dd in days["date"]:
        print(cow, dd)
        # select data
        data_x = data.loc[(data["cowid"] == cow) &
                          (data["date"] == dd),"X"]
        data_y = data.loc[(data["cowid"] == cow) &
                          (data["date"] == dd),"y"]
        
        # set limits (based on information barn)
        barnnr = int(data.loc[(data["cowid"] == cow) &
                          (data["date"] == dd),"barn"].mean())
        x_lim = barn[str(barnnr)] #[data_x.min(),data_x.max()]
        y_lim = [-18,0] #[data_y.min(),data_y.max()]
        
        # plot heatmap
        matplotlib.rcdefaults() 
        fig, ax = plt.subplots() 
        ax = heatmap_barn(data_x, data_y, xstep, ystep, x_lim, y_lim)
        ax.set_xlabel('y coordinate')
        ax.set_ylabel('x coordinate')
        ax.set_title("barn = " +str(barnnr) + ', cow = ' + str(cow) + ", date = " + datetime.strftime(dd,"%Y-%m-%d").replace("-",""))
        ax.xaxis.tick_top()
        plt.savefig(path_out + "//heatmap_" + str(cow) + "_" + datetime.strftime(dd,"%Y-%m-%d").replace("-",""))
        plt.close()
"""

def heatmap_barn2(
    data_x, data_y, xstep, ystep, x_lim, y_lim, image_data=False, **kwargs
):
    """
    Plot the frequency of positions registered, given data_x and data_y
    xstep and ystep determine how fine these positions are taken
    x_lim and y_lim set boundaries (e.g. incl feeding lane)

    Parameters
    ----------
    data_x : TYPE = Pandas series
        Data of the x-coordinate as registered by the sewio system
    data_y : TYPE = Pandas series
        Data of the y-coordinate as registered by the sewio system
    xstep : TYPE = float
        How fine you want to determine location in x-direction (e.g. 1 is one
            meter, 0.1 = 10 centimeters)
    ystep : TYPE = float
        How fine you want to determine location in x-direction (e.g. 1 is one
            meter, 0.1 = 10 centimeters)
    x_lim : TYPE = list of floats
        Physical boundaries of plot you want to make (e.g. cut off feeding lane
            then indicate lower boundary in y direction = -4)
    y_lim : TYPE = list of floats
        Physical boundaries of plot you want to make (e.g. cut off feeding lane
            then indicate lower boundary in y direction = -4)
    image_data : TYPE = bool
        Do X,Y data originate from an image?

    Returns
    -------
    ax : TYPE = sns heatmap
        figure axes with heatmap

    """
    # transform data_x and data_y for count
    x_transf = (data_x * (1 / xstep)).round() * xstep
    y_transf = (data_y * (1 / ystep)).round() * ystep

    # put x_transf and y_transf in DataFrame
    frame = {"xT": x_transf.round(decimals=4), "yT": y_transf.round(decimals=4)}
    df = pd.DataFrame(frame)

    # count and groupby
    heatdata = df.groupby(["xT", "yT"]).size()
    heatdata = heatdata.to_frame(name="counts")
    heatdata = heatdata.reset_index()

    # set limits to agree with steps
    x_low = round(round(x_lim[0] * (1 / xstep)) * xstep, ndigits=4)
    x_high = round(round(x_lim[1] * (1 / xstep)) * xstep, ndigits=4)
    y_low = round(round(y_lim[0] * (1 / ystep)) * ystep, ndigits=4)
    y_high = round(round(y_lim[1] * (1 / ystep)) * ystep, ndigits=4)

    # delete the rows for which values are outside x_lim or y_lim
    heatdata = heatdata.loc[
        (heatdata["xT"] >= x_lim[0])
        & (heatdata["xT"] <= x_lim[1])
        & (heatdata["yT"] >= y_lim[0])
        & (heatdata["yT"] <= y_lim[1])
    ]
    heatdata.reset_index(drop=True, inplace=True)

    # expected axes limits based on lim and step
    x = np.arange(x_low, x_high + xstep, xstep, dtype=float)
    y = np.arange(y_low, y_high + ystep, ystep, dtype=float)

    # Create rectangular dataframe (x_high, y_high)
    if image_data:
        temp_holder = pd.DataFrame(
            data={
                "xT": pd.Series(
                    [
                        int(i)
                        for i in np.arange(x_low, x_high + 1, xstep, dtype=float)
                        for _ in np.arange(y_low, y_high + 1, ystep, dtype=int)
                    ]
                ),
                "yT": np.tile(
                    np.arange(y_low, y_high + 1, ystep, dtype=int),
                    len(np.arange(x_low, x_high + 1, xstep, dtype=float)),
                ),
            }
        )
    else:
        temp_holder = pd.DataFrame(
            data={
                "xT": pd.Series(
                    [
                        i
                        for i in np.arange(x_low, x_high, xstep, dtype=float)
                        for _ in np.arange(y_low, y_high, ystep, dtype=float)
                    ]
                ),
                "yT": np.tile(
                    np.arange(y_low, y_high, ystep, dtype=float),
                    len(np.arange(x_low, x_high, xstep, dtype=float)),
                ),
            }
        )
        
    # 
    temp_holder["yT"] = round(temp_holder["yT"],1)
    temp_holder["xT"] = round(temp_holder["xT"],1)
    heatdata["yT"] = round(heatdata["yT"],1)
    heatdata["xT"] = round(heatdata["xT"],1)
    

    # merge into frame
    tdf = pd.merge(temp_holder, heatdata, how="left", on=["xT", "yT"])
    tdf["counts"] = tdf["counts"].fillna(0)
    tdf = tdf.pivot(index="yT", columns="xT", values="counts").T
    
    # convert to df with right colnames and indices
    xcols = np.array(x.round(decimals=3),dtype = str)
    ycols = np.flip(np.array(y.round(decimals=3),dtype = str))
    # df = pd.DataFrame(tdf, columns = ycols, index = xcols)
    

    defaultkwargs = {'yticklabels' : round(1 / ystep),
                     'xticklabels' : round(1 / xstep * 2),
                     'cmap' : plt.cm.YlOrRd_r,}

    kwargs = {**defaultkwargs, **kwargs}

    # make heatmap
    ax = sns.heatmap(
        tdf,
        yticklabels = round(1/xstep),
        xticklabels = round(1/ystep*2),
        robust = True,
        cbar = False,
        cmap = "Greys"
    )




    return ax
