# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 10:59:32 2023

@author: adria036
"""

import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\nlas\scripts\preprocessing_uwb") 


#%% import modules

from datetime import date, timedelta
import pandas as pd
import numpy as np
from filter_data import filter_interp_withacc
from read_data_sewio_LF import read_ids, combine_data_id, read_ids_LF, read_all_data_hour, read_all_data_hour_extra
from uwbfunctions import behaviour_bouts


#%% set filepaths and dates

# set path to data X,y
path_os = os.path.join(
    "W:","\ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_sewio"
    )

# set path to excel file with cowids
path = path_os

# set path to results folder
path_res = os.path.join(
    "W:","ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","uwb_processed"
    )

# set file name for cowid information
fn = "Copy_of_Sewio_tags.xlsx"

# set start and end dates
startdate =  date(2021,12,1) #date.today() - timedelta(days = 1)
enddate =  date(2021,12,1) #date.today() - timedelta(days = 1)

# set parameters for the imputation and interpolation
win_med = 11
gap_thres = 180


#%% write loop for data loading, editing and saving
t_interval = enddate - startdate
# read zones for area assignment
fna = "barn_areas.xlsx"
area_zones = pd.read_excel(os.path.join(path,fna), sheet_name = "areas")   
area_zones2 = pd.read_excel(os.path.join(path,fna), sheet_name = "areas2")   
barn_edges = pd.read_excel(os.path.join(path,fna), sheet_name = "edges")

for dd in range(t_interval.days+1):
    dd_0 = timedelta(dd-1)
    dd = timedelta(dd)
    print(startdate+dd)
    
    # --------------------------read & select data-----------------------------
    try:
        # -----------------------read the data---------------------------------
        data = read_all_data_hour(startdate+dd,startdate+dd,path_os)
        data_extra = read_all_data_hour_extra(startdate+dd_0, path_os)
        
        data = pd.concat([data_extra,data])
        data = data.sort_values(by = ["alias","feed_reference","at"]).reset_index(drop=1)
        del data_extra        
        # add selection step to correct for time > only day "startdate + dd" retained
        data = data.loc[data["date"].dt.date == startdate+dd,:]
        data = data.reset_index(drop=1)
    
        # ------------------------if no position data------------------------------
        # read alias from file
        alias = pd.read_excel(os.path.join(path_os,fn), sheet_name = "alias")
        data = pd.merge(data,alias,how = "outer", on = "title")
        data = data.drop(columns = ["alias_x"])
        data = data.rename(columns = {"alias_y":"alias"})
        # delete rows of alias with no data
        data = data.loc[~data["alias"].isna(),:]
        data = data.loc[~data["at"].isna(),:]
        
        #-------------------------data selection 1/s---------------------------            
        # summarize data (X,y,acc) per unique second (0 to 86399)
        data["day"] = data["date"].dt.day
        data["hour"] = data["at"].dt.hour - data["date"].dt.hour
        data.loc[data["hour"]<0,"hour"] = 24 + data.loc[data["hour"]<0,"hour"]
        data["minute"] = data["at"].dt.minute
        data["second"] = data["at"].dt.second
        data["relsec"] = data["hour"]*3600 \
                         + data["minute"]*60 \
                         + data["second"]
        
        # summarize acc and position data
        new = data[["alias","feed_reference","date","relsec","acc_x","acc_y","acc_z","X","y"]].groupby(by = ["alias","feed_reference","date","relsec"]).mean().reset_index()
        new = new.sort_values(by = ["alias","feed_reference","relsec"])
        if "X" not in new.columns:
            new["X"] = np.nan
        if "y" not in new.columns:
            new["y"] = np.nan
            
        idx = data[["alias","feed_reference","date","relsec"]].drop_duplicates()        
        new["at"] = data.loc[idx.index.values,:]["at"].values
        new["alias"] = pd.to_numeric(new["alias"])
        new = new.drop(columns = ["date"],axis =1)
        new["date"] = pd.to_datetime(new["at"].dt.date, format = "%Y-%m-%d")
        del data, idx
        
        # 'new' contains one record (acc_x, acc_y, acc_z, X, y) per second per tag
        # this frame can be used to add tags to cow ids         
        #----------------------------------------------------------------------
        
        # read id/tag data
        tags = read_ids(path, fn)
        tagsLF = read_ids_LF(path, fn)    
        tags = pd.concat([tags,tagsLF],axis = 0)
        tags = tags.sort_values(by = ["cow","datetime_begin"]).reset_index(drop=1)
        tags["datetime_end"] = tags["datetime_end"].replace(" ","NaT")
        del tagsLF
    
        # combine it with data
        data,no_ids = combine_data_id(new,tags)
        if len(no_ids)>0:
            no_ids.to_csv(os.path.join(path_res,"missing_id_records",str(startdate).replace("-","") + "_missing_ids.txt"), index=False)
        del no_ids,new
        
        # remaining tags without cowID = during the changing
            # remove those values
        data = data.loc[data["cowid"] != 0,:]
        data = data.sort_values(by=["cowid","at"]).reset_index(drop=1)
        data = data[["cowid","alias","feed_reference","barn","date","at","relsec","X","y","acc_x","acc_y","acc_z"]]  
        
        # unfiltered data - one per second or less, all cows included  
        # calculate gaps based on numeric time relative to start of dataset
        data["gap"] = data["relsec"].diff()
        data.loc[data["gap"] < 0,"gap"] = np.nan # set to nan when different cowid
        
        # prepare write data to file with all cows per day
        alldata = pd.DataFrame([])
        
        #-----------------------filter & interpolate per cow-----------------------
        # filter the (X,y) time series with a median filter, minimal dist travelled and interpolate
        # cows in the barn
        cows = data["cowid"].drop_duplicates().sort_values().reset_index(drop=1)
        cows = pd.DataFrame(cows)
        
        # loop over all data per cowid and select (x,y,t) data to filter
        for i in range(0,len(cows)):
            # select data
            #print("i = " + str(i) + ", cow = " + str(round(cows["cowid"][i])) + ", barn = " +str(data.loc[(data["cowid"] == cows["cowid"][i]),"barn"].mean()))
            cow = cows["cowid"][i]
            cow_t = data.loc[(data["cowid"] == cows["cowid"][i]),"relsec"]
            cow_x = data.loc[(data["cowid"] == cows["cowid"][i]),"X"]
            cow_y = data.loc[(data["cowid"] == cows["cowid"][i]),"y"]
            acc_x = data.loc[(data["cowid"] == cows["cowid"][i]),"acc_x"]
            acc_y = data.loc[(data["cowid"] == cows["cowid"][i]),"acc_y"]
            acc_z = data.loc[(data["cowid"] == cows["cowid"][i]),"acc_z"]
            barn = round(data.loc[(data["cowid"] == cows["cowid"][i]),"barn"].mean())
            
            # filter data
            if len(cow_t) > 10:
                # add edges if t doesn't end at second 86399 of the day
                if cow_t.max() < 86399:
                    cow_t = pd.concat([cow_t,pd.Series(86399)], axis=0)
                    cow_x = pd.concat([cow_x,pd.Series(np.nan)], axis=0)
                    cow_y = pd.concat([cow_y,pd.Series(np.nan)], axis=0)
                    acc_x = pd.concat([acc_x,pd.Series(np.nan)], axis=0)
                    acc_y = pd.concat([acc_y,pd.Series(np.nan)], axis=0)
                    acc_z = pd.concat([acc_z,pd.Series(np.nan)], axis=0)
                
                # add edges if t doesn't start with 0th second of the day
                if cow_t.min() > 0:
                    cow_t = pd.concat([pd.Series(0),cow_t], axis=0)
                    cow_x = pd.concat([pd.Series(np.nan),cow_x], axis=0)
                    cow_y = pd.concat([pd.Series(np.nan),cow_y], axis=0)
                    acc_x = pd.concat([pd.Series(np.nan),acc_x], axis=0)
                    acc_y = pd.concat([pd.Series(np.nan),acc_y], axis=0)
                    acc_z = pd.concat([pd.Series(np.nan),acc_z], axis=0)
                
                cow_t = cow_t.reset_index(drop=1)
                cow_x = cow_x.reset_index(drop=1)
                cow_y = cow_y.reset_index(drop=1)
                acc_x = acc_x.reset_index(drop=1)
                acc_y = acc_y.reset_index(drop=1)
                acc_z = acc_z.reset_index(drop=1)
                
                # filter barn edges if within 0.5m of edge
                edges = barn_edges.loc[(barn_edges["barn"] == barn),:].reset_index(drop=1)
                x1,y1,x2,y2,x3,y3,x4,y4 = edges[["x1","y1","x2","y2","x3","y3","x4","y4"]].values[0]
                test2 =  cow_x.loc[((cow_x > (x1-0.5)) & (cow_x < x1))] = x1  # left edge x
                cow_x.loc[(cow_x > x2) & (cow_x < x2+0.5)] = x2  # right edge x
                cow_y.loc[(cow_y < y1) & (cow_y > y1-0.5)] = y1  # upper edge y
                cow_y.loc[(cow_y > y3)] = -1.2  # upper edge y
                del x1,y1,x2,y2,x3,y3,x4,y4,edges
                
                # filter with acceleration
                df,x,y = filter_interp_withacc(cow_t,cow_x,cow_y,acc_x,acc_y,acc_z,win_med,gap_thres)
                # select columns and add cowid
                df["cowid"] = cows["cowid"][i]
                df["barn"] = barn
                df["date"] = str(startdate+dd)
                df.loc[df["xnew"].isna(),"xnew"] = df.loc[df["xnew"].isna(),"X"]
                df.loc[df["ynew"].isna(),"ynew"] = df.loc[df["ynew"].isna(),"y"]
                
                #--------------------------------------------------------------
                # if origin of system not changed, can be calculated on all
                if pd.to_datetime(startdate+dd) >= pd.to_datetime("2022-09-01"):
                    myarea = area_zones.loc[(area_zones["barn"] == barn) | \
                                            (area_zones["barn"].isna() == True) \
                                            ,:].reset_index(drop=1)
                    for j in range(0,len(myarea)):
                        x1,y1,x2,y2,x3,y3,x4,y4 = myarea[["x1","y1","x2","y2","x3","y3","x4","y4"]].values[j]
                        df.loc[((df["xnew"] >= x1) & (df["xnew"] <= x2) & \
                               (df["xnew"] >= x3) & (df["xnew"] <= x4) & \
                               (df["ynew"] >= y1) & (df["ynew"] >= y2) & \
                               (df["ynew"] <= y3) & (df["ynew"] <= y4)),"area"] = myarea["area"][j]
                        df.loc[((df["xnew"] >= x1) & (df["xnew"] <= x2) & \
                               (df["xnew"] >= x3) & (df["xnew"] <= x4) & \
                               (df["ynew"] >= y1) & (df["ynew"] >= y2) & \
                               (df["ynew"] <= y3) & (df["ynew"] <= y4)),"zone"] = myarea["zone"][j]
                        del x1,y1,x2,y2,x3,y3,x4,y4
                else:
                    myarea = area_zones2.loc[(area_zones2["barn"] == barn) | \
                                            (area_zones2["barn"].isna() == True) \
                                            ,:].reset_index(drop=1)
                    for j in range(0,len(myarea)):
                        x1,y1,x2,y2,x3,y3,x4,y4 = myarea[["x1","y1","x2","y2","x3","y3","x4","y4"]].values[j]
                        df.loc[((df["xnew"] >= x1) & (df["xnew"] <= x2) & \
                               (df["xnew"] >= x3) & (df["xnew"] <= x4) & \
                               (df["ynew"] >= y1) & (df["ynew"] >= y2) & \
                               (df["ynew"] <= y3) & (df["ynew"] <= y4)),"area"] = myarea["area"][j]
                        df.loc[((df["xnew"] >= x1) & (df["xnew"] <= x2) & \
                               (df["xnew"] >= x3) & (df["xnew"] <= x4) & \
                               (df["ynew"] >= y1) & (df["ynew"] >= y2) & \
                               (df["ynew"] <= y3) & (df["ynew"] <= y4)),"zone"] = myarea["zone"][j]
                        del x1,y1,x2,y2,x3,y3,x4,y4   
                        
                df = df[["cowid","barn","date","t","gap","X","y","xnew","ynew","acc_x","acc_y","acc_z","dist","area","zone"]]
                alldata = pd.concat([alldata,df])
                #test = df.loc[(df["area"].isna()) & (df["dist"] < 180),:]   # check no area
                #plt.scatter(test["xnew"],test["ynew"],marker = "o",c = "blue",s = 4)
                del x, y
                
                df.to_csv(os.path.join(path_res,"barn"+str(round(barn)), "data_" + str(startdate+dd).replace("-","") + "_barn_" + str(round(barn)) + "_cow_"+ str(cows["cowid"][i])  + ".txt"), index=False)
                del cow_t, cow_x, cow_y, acc_x, acc_y, acc_z
                
                #--------------------------------------------------------------
                ######################CALCULATE BOUTS##########################
                #--------------------------------------------------------------
                
                # calculate and summarize behaviours and save 1 file per day per cow
                bouts = pd.DataFrame([])
                bsums = pd.DataFrame([])
                act_dist = pd.DataFrame([])
                dset = df.copy()
                dset["area"] = dset["area"].fillna('')
                               
                # ------------------------ feeding behaviour --------------------------
                interval = 5*60      # min number of seconds between separate bouts
                behaviour = "feed"   # feeding behaviour
                min_length = 30      # do not count if shorter than 30 seconds
                df,summary = behaviour_bouts(dset, interval, behaviour,min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # ---------------------- drinking behaviour ---------------------------
                interval = 5*60      # min number of seconds between separate bouts
                behaviour = "drink"   # drinking behaviour
                min_length = 5      # do not count if shorter than 2 seconds
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # ---------------------- resting behaviour ----------------------------
                interval = 2*60      # min number of seconds between separate bouts
                behaviour = "cubicle"   # resting behaviour
                min_length = 30      # do not count if shorter than 30 seconds
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # -------------------------- cubicle_A --------------------------------
                interval = 2*60      # min number of seconds between separate bouts
                behaviour = "cubicle_A"   # resting behaviour
                min_length = 30      # do not count if shorter than 30 seconds
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # -------------------------- cubicle_B --------------------------------
                interval = 2*60      # min number of seconds between separate bouts
                behaviour = "cubicle_B"   # resting behaviour
                min_length = 30      # do not count if shorter than 30 seconds
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # -------------------------- cublicle_C -------------------------------
                interval = 2*60      # min number of seconds between separate bouts
                behaviour = "cubicle_C"   # resting behaviour
                min_length = 30      # do not count if shorter than 30 seconds
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # ------------------------ concentrate feeder -------------------------
                interval = 5*60      # min number of seconds between separate bouts
                behaviour = "concentrate"   # concentrate feeding behaviour
                min_length = 0       # always count
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # ------------------------ waiting area ------------------------------
                interval = 20*60      # min number of seconds between separate bouts
                behaviour = "wait"   # in the waiting area
                min_length = 0        # always count
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # ------------------------ unknown area ------------------------------
                interval = 5*60      # min number of seconds between separate bouts
                behaviour = "unknown"   # unknown behaviour
                min_length = 10      # do not count if shorter than 10 seconds
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # ------------------------- slatted (barn 62) -------------------------
                interval = 5*60      # min number of seconds between separate bouts
                behaviour = "slatted"   # unknown behaviour
                min_length = 10      # do not count if shorter than 10 seconds
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # ------------------------- resting (barn 62) -------------------------
                interval = 5*60      # min number of seconds between separate bouts
                behaviour = "resting"   # unknown behaviour
                min_length = 10      # do not count if shorter than 10 seconds
                df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
                bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
                bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
                
                # # ------------------------- distance travelled ------------------------
                dset["dist"] = 0
                dset.iloc[1:,dset.columns == "dist"] = \
                        np.sqrt((dset["xnew"].iloc[:-1].values-dset["xnew"].iloc[1:].values)**2 + \
                                (dset["ynew"].iloc[:-1].values-dset["ynew"].iloc[1:].values)**2)
                dset.loc[(dset["dist"]<0.30) | (dset["dist"]>5),"dist"] = 0
                
                # distance travelled and % active (with and without nans)
                act = dset[["cowid","date","barn","dist"]].groupby(by = ["cowid","date","barn"]).sum().reset_index()
                act["dist"] = round(act["dist"],2)
                act["perc_act"] = round(len(dset.loc[dset["dist"] > 0])/len(dset)*100,2)
                act["perc_act_nan"] = round(len(dset.loc[dset["dist"] > 0])/(0.0001+len(dset.loc[dset["area"]!="unknown"]))*100,2)
                
                # % of the day not in the cubicles
                act["perc_no_lying"] = 100- round(len(dset.loc[(dset["area"].str.contains("cubicle") == True) | \
                                                     (dset["area"].str.contains("resting") == True)])/len(dset)*100,2)
                act["perc_no_lying_nan"] = 100- round(len(dset.loc[(dset["area"].str.contains("cubicle") == True) | \
                                                     (dset["area"].str.contains("resting") == True)])/(0.0001+len(dset.loc[dset["area"]!="unknown"]))*100,2)
                
                act_dist = pd.concat([act_dist,act]).reset_index(drop=1)
                
                # save bouts, summaries and behaviours per cow id
                act_dist["dist"] = round(act_dist["dist"],2)
                act_dist["perc_no_lying_nan"] = round(act_dist["perc_no_lying_nan"],2)
                act_dist["perc_no_lying"] = round(act_dist["perc_no_lying"],2)
                
                # save with append if doesn't exist
                if not os.path.isfile(os.path.join(path_res,"activity_cow_" + str(cow) + "_barn" + str(barn) + ".txt")):
                    act_dist.to_csv(os.path.join(path_res,"activity_cow_" + str(cow) + "_barn" + str(barn) + ".txt"), index=False)
                else:
                    act_dist.to_csv(os.path.join(path_res,"activity_cow_" + str(cow) + "_barn" + str(barn) + ".txt"), index=False,mode = 'a',header=False)
                
                if not os.path.isfile(os.path.join(path_res,"bouts_cow_" + str(cow) + "_barn" + str(barn) + ".txt")):
                    bouts.to_csv(os.path.join(path_res, "bouts_cow_" + str(cow) + "_barn" + str(barn) + ".txt"), index=False)
                else:
                    bouts.to_csv(os.path.join(path_res, "bouts_cow_" + str(cow) + "_barn" + str(barn) + ".txt"), index=False,mode = 'a',header = False)
                
                if not os.path.isfile(os.path.join(path_res, "summary_cow_" + str(cow) + "_barn" + str(barn) + ".txt")):
                    bsums.to_csv(os.path.join(path_res,"summary_cow_" + str(cow) + "_barn" + str(barn) + ".txt"), index=False)
                else: 
                    bsums.to_csv(os.path.join(path_res,"summary_cow_" + str(cow) + "_barn" + str(barn) + ".txt"), index=False,mode = 'a',header=False)
                
            else:
                df = pd.DataFrame({"cowid":pd.Series(np.ones(86400)*cows["cowid"][i]),
                                   "barn":pd.Series(np.ones(86400)*barn),
                                   "date":str(startdate+dd),
                                   "t" : pd.Series(np.arange(0,86400,1)),
                                   "gap" :  pd.Series(np.arange(0,86400,1)*np.nan),
                                   "X" :  pd.Series(np.arange(0,86400,1)*np.nan),
                                   "y" :  pd.Series(np.arange(0,86400,1)*np.nan),
                                   "xnew" : pd.Series(np.arange(0,86400,1)*np.nan),
                                   "ynew" : pd.Series(np.arange(0,86400,1)*np.nan),
                                   "acc_x" :  pd.Series(np.arange(0,86400,1)*np.nan),
                                   "acc_y" :  pd.Series(np.arange(0,86400,1)*np.nan),
                                   "acc_z" :  pd.Series(np.arange(0,86400,1)*np.nan),
                                   "dist" : pd.Series(np.arange(0,86400,1)*np.nan),
                                   "area" : pd.Series(np.arange(0,86400,1)*np.nan),
                                   "zone" : pd.Series(np.arange(0,86400,1)*np.nan)
                                   })
                alldata = pd.concat([alldata,df])
                
                # write data to csv per cow
                df.to_csv(os.path.join(path_res,"barn"+str(round(barn)), "data_" + str(startdate+dd).replace("-","") + "_barn_" + str(round(barn)) + "_cow_"+ str(cows["cowid"][i])  + ".txt"), index=False)
                
                del cow_t, cow_x, cow_y, acc_x, acc_y, acc_z
                

            del df
            
            
        # clear workspace and memory
        alldata.to_csv(os.path.join(path_res,"alldata_" + str(startdate+dd).replace("-","") + ".txt" ))
        del data
    except:
        print("data preprocessing for " + str(startdate+dd).replace("-","") + " has failed ")
        pass
    

    
