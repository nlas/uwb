# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 13:51:49 2023

@author: adria036
"""
import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\nlas\scripts\uwb") 


#%% import modules


from datetime import date, datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.patches import Rectangle
import seaborn as sns
from uwbfunctions import heatmap_barn,heatmap_barn2
#%matplotlib qt

#%% set paths and constants and load data

# path_out 
svpath = r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\nlas\results\exploration"



# path to per barn directories
path = os.path.join("W:","\ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","uwb_processed")
path_zones = os.path.join("W:","\ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_sewio")
fna = "\\Barn_areas.xlsx"
area_zones = pd.read_excel(path_zones+fna, sheet_name = "areas")    
barn_edges = pd.read_excel(path_zones+fna, sheet_name = "edges")
del fna, path_zones


# settings
settings = {'barn' : [60,61,62,70,71,72,73], # [60,61,62,70,71,72,73],
            'startdate' : date(2022,1,1),
            'enddate' :  date(2023,2,28),
            'cows' : 0#[907,89,98,517,1748,495,3107], # or specific cow number
            }

# files that comply with settings
fn = []
for b in range(0,len(settings["barn"])):
    print("barn = " + str(settings["barn"][b]))
    if settings["cows"] == 0:
        fbarn = [f for f in os.listdir(path + "/barn" + str(settings["barn"][b])) \
                if os.path.isfile(os.path.join(path,"barn"+str(settings["barn"][b]),f)) \
                    and (datetime.strptime(f[5:13], '%Y%m%d').date() >= settings["startdate"]) \
                    and (datetime.strptime(f[5:13], '%Y%m%d').date() <= settings["enddate"])]
        fbarn.sort()
    else:
        fbarn = [f for f in os.listdir(path + "/barn" + str(settings["barn"][b])) \
                if os.path.isfile(os.path.join(path,"barn"+str(settings["barn"][b]),f)) \
                    and (int(f[26:-4]) in settings["cows"]) \
                    and (datetime.strptime(f[5:13], '%Y%m%d').date() >= settings["startdate"]) \
                    and (datetime.strptime(f[5:13], '%Y%m%d').date() <= settings["enddate"])]
        fbarn.sort()
    fn.extend(fbarn)
    fn.sort()

# find unique cows
cows = list(set([int(f[26:-4]) for f in fn]))

# read data
data = pd.DataFrame([])
for f in fn:
    barn = f[19:21]
    sub = pd.read_csv(path + "/barn" + barn + "/" + f, 
                      usecols = ["cowid","barn","date","t","xnew","ynew","area","zone","X","y"],
                      dtype = {"cowid" : "int64","barn" : "int64","date" : "object",
                               "t" : "int64", "xnew":"float64","ynew":"float64",
                               "area":"object","zone":"float64","X":"float64","y" : "float64"})
    sub["date"] = pd.to_datetime(sub["date"], format = "%Y-%m-%d") 
    data = pd.concat([data,sub])
data = data.sort_values(by = ["cowid","date","t"])
data = data.reset_index(drop=1)

# correct the areas
cows = data["cowid"].drop_duplicates().reset_index(drop=1)
for cow in cows:
    print(cow)
    days = data.loc[(data["cowid"]==cow),["date"]].drop_duplicates() 
    days = days.sort_values(by = ["date"]).reset_index(drop=1)
    for dd in days["date"]:
        print(dd)
        barnnr = int(data.loc[(data["cowid"] == cow) &
                          (data["date"] == dd),"barn"].mean()) 
        myarea = area_zones.loc[(area_zones["barn"] == barnnr) | \
                            (area_zones["barn"].isna() == True) \
                            ,:].reset_index(drop=1)
        for j in range(0,len(myarea)):
            x1,y1,x2,y2,x3,y3,x4,y4 = myarea[["x1","y1","x2","y2","x3","y3","x4","y4"]].values[j]
            data.loc[((data["cowid"] == cow) & (data["date"] == dd) & \
                      (data["xnew"] >= x1) & (data["xnew"] <= x2) & \
                      (data["xnew"] >= x3) & (data["xnew"] <= x4) & \
                      (data["ynew"] >= y1) & (data["ynew"] >= y2) & \
                      (data["ynew"] <= y3) & (data["ynew"] <= y4)),"area"] = myarea["area"][j]
            data.loc[((data["cowid"] == cow) & (data["date"] == dd) & \
                      (data["xnew"] >= x1) & (data["xnew"] <= x2) & \
                      (data["xnew"] >= x3) & (data["xnew"] <= x4) & \
                      (data["ynew"] >= y1) & (data["ynew"] >= y2) & \
                      (data["ynew"] <= y3) & (data["ynew"] <= y4)),"zone"] = myarea["zone"][j]
        del x1,y1,x2,y2,x3,y3,x4,y4
del cows, dd, days
data.loc[data["zone"].isna(),"zone"] = 8
data.loc[data["zone"] == 8 ,"area"] = "unknown"

data["date"] = data["date"].dt.date

# check correctly read ids
print(data[["cowid","barn"]].drop_duplicates())

#%% summarize data

""" areas
areas / zones
zone = 0 : walking area
zone = 1 : cublicles a, b, c
zone = 2 : feed_rack
zone = 3 : drink_trough
zone = 4 : concentrate feeder
zone = 5 : slatted area of barn 62
zone = 6 : resting area of barn 62
zone = 7 : waiting area
zone = 8 : no area assigned
"""

# summarize data per zone
zones = data[["cowid","date","area","t"]].groupby(by = ["cowid","date","area"]).count().reset_index()
zones = zones.sort_values(by = ["cowid","area"])

# time spent per zone
cows = data["cowid"].drop_duplicates()
for cow in cows:
    subset = data.loc[data["cowid"]==cow,["cowid","barn","date","t","xnew","ynew","area","zone"]]
    barnnr = int(data.loc[(data["cowid"] == cow),"barn"].mean())                      
    sumdata = subset[["date","zone","cowid"]].groupby(by=["date","zone"]).count().reset_index()
    if barnnr != 62:
        areas = pd.DataFrame(['walk','cubicle','feed','drink','concentrate','waiting_area','unknown'],columns = ["area"])
        areas["zone"] = [0,1,2,3,4,7,8] 
        sumdata = sumdata.merge(areas,on = "zone")
    else:
        areas = pd.DataFrame(['feed','drink','concentrate','slatted','resting','waiting_area','unknown'],columns = ["area"])
        areas["zone"] = [2,3,4,5,6,7,8] 
        sumdata = sumdata.merge(areas,on = "zone")
    sumdata["percentage"] = round(sumdata["cowid"]/864,2)
    sumdate = sumdata.sort_values(by = ["date","zone"]).reset_index(drop=1)
    #sumdata2=sumdata.groupby(by="date").sum()
    
    
    #fig,ax = plt.subplots(nrows=1,ncols=1,figsize=(20,10))
    sns.set(style="ticks")
    sns.set_style("whitegrid")
    g = sns.catplot(data=sumdata,x="date",y = "percentage", col="area",kind="bar",height=5.5,aspect=0.7,palette = 'magma')
    #ax.legend(["walk","cub_a","cub_b","cub_c","feed","drink","conc"])
    g.set_axis_labels("", "Percentage area usage")
    g.set_xticklabels(sumdata.date.drop_duplicates(),rotation=45,horizontalalignment='right')
    g.set_titles("{col_name}")
    g.fig.suptitle("cow " + str(cow) + ", barn " + str(barnnr))
    g.fig.subplots_adjust(top = 0.85)
    g.savefig(svpath + "\\timebudgets_cow"+str(cow) + "_barn" + str(barnnr) + ".png")
    plt.close()


# # summarize variables
# data[["xnew","X","ynew","y"]].describe()
# data[""]
# test = data.loc[(data["ynew"] < -17) & (data["ynew"] > -19) & (data["cowid"] = cow),:]
# sns.scatterplot(data=test, x = test.index.values, y = "ynew")
# sns.scatterplot(data=test, x = test.index.values, y = "y")



#%% data exploration and visualisation

#TODO
"""
     - explore the number of cows with data on each day
     - explore when no areas are assigned and so, wrong barn is entered
     - explore gaps and gapsize
"""
  
#-------------------------------gapsize----------------------------------------
subset = data.loc[data["X"].isna(),["cowid","barn","date","t"]]
sumdata = subset.groupby(by = ["cowid","barn","date"]).count()
subset = data.loc[data["xnew"].isna(),["cowid","barn","date","t"]]
test = subset.groupby(by = ["cowid","barn","date"]).count()
sumdata["xnew"] = test["t"]

sumdata["gapperc"] = sumdata["t"]/864
sumdata["gap180s"] = sumdata["xnew"]/864
sumdata = sumdata.reset_index()
sumdata = sumdata.sort_values(by = ["date","cowid"]).reset_index(drop=1)
sumdata["xlabel"] = np.arange(len(sumdata))


plt.figure()
ax = sns.scatterplot(data=sumdata,x = "xlabel", y = "gapperc", 
                     palette = "tab10", hue = "cowid", 
                     style = "date", legend = False)
plt.savefig(svpath + "\\gap_notimputed.png")
plt.figure()
ax = sns.scatterplot(data=sumdata,x = "xlabel", y = "gap180s", 
                     palette = "tab10", hue = "cowid", 
                     style = "date", legend = False)
plt.savefig(svpath + "\\gap_imputed.png")


#------------------------------behaviour over 24h------------------------------
#sns.set_style("darkgrid") # whitegrid, dark, white, ticks

cows = data["cowid"].drop_duplicates().reset_index(drop=1)
for cow in cows:
    print(cow)
    days = data.loc[(data["cowid"]==cow),["date"]].drop_duplicates() 
    days = days.sort_values(by = ["date"]).reset_index(drop=1)
    
    for dd in days["date"]:
        print(dd)
        subset = data.loc[(data["cowid"]==cow) & \
                          (data["date"]==dd),:].copy()
        palette = {"walking" : "darkorange",
                   "concentrate " : "tan",
                   "cubicle_A" : "darkorchid",
                   "cubicle_B" : "violet",
                   "cubicle_C" : "palevioletred",
                   "feed_rack" : "crimson",
                   "drink_trough" : "gold",
                   "slatted" : "limegreen",
                   "resting" : "darkorchid",
                   "waiting_area" : "mediumseagreen",
                   "unknown" : "royalblue"}
        fig,ax = plt.subplots(1,1,figsize = (18,7))
        sns.scatterplot(data=subset,x="t",y="zone",
                        hue="area",
                        marker = "s", linewidth = 0,
                        palette = palette,
                        legend = False)
        sns.lineplot(data=subset, x='t',y = 'zone', 
                     linewidth = 0.2, color = 'k')
        ax.set_yticks(range(9))
        ax.set_yticklabels(["walking","cubicles","feed rack","drink trough",
                            "concentrate feeder","slatted (b62)","resting (b62)",
                            "waiting area","unknown"])
        sns.set_style("ticks")
        ax.set_xticks([0,14400,28800,43200,57600,72000,86400])
        ax.set_xticklabels(['00:00','04:00','08:00','12:00','16:00','20:00','24:00'])
        ax.set_xlabel("time [h]")
        ax.set_title("cow = " + str(cow) + ', date = ' + str(dd))
        ax.set_xlim([-1000,87400])
        plt.savefig(svpath+"\\timebudget_cow_" + str(cow) + "_" + str(dd).replace("-","") + ".png")
        plt.close()
        
        
# ------------------------- individual cow heatmaps ---------------------------
cows = data["cowid"].drop_duplicates().reset_index(drop=1)

#barn limits
barn = {"60":[64.8,75.6],
        "61":[54,64.8],
        "62":[32.4,54],
        "70":[21.6,32.4],
        "71":[10.8,21.6],
        "72":[0,10.8],
        "73":[-10.8,0],
        }

# plots per cow-day
for cow in cows["cowid"]:
    
    # unique days
    days = data.loc[(data["cowid"]==cow),["date"]].drop_duplicates() 
    days = days.sort_values(by = ["date"]).reset_index(drop=1)
    
    # select data and create heatmaps
    for dd in days["date"]:
        print(cow, dd)
        # select data
        data_x = data.loc[(data["cowid"] == cow) &
                          (data["date"] == dd),"X"]
        data_y = data.loc[(data["cowid"] == cow) &
                          (data["date"] == dd),"y"]
        
        # set limits (based on information barn)
        barnnr = int(data.loc[(data["cowid"] == cow) &
                          (data["date"] == dd),"barn"].mean())
        x_lim = barn[str(barnnr)]
        y_lim = [-18,0]
        
        # plot heatmap
        # matplotlib.rcdefaults() 
        fig, ax = plt.subplots() 
        ax = heatmap_barn(data_x, data_y, 0.1, 0.1, x_lim, y_lim)
        ax.set_xlabel('y coordinate')
        ax.set_ylabel('x coordinate')
        ax.set_title("barn = " +str(barnnr) + ', cow = ' + str(cow) + \
                     ", date = " + datetime.strftime(dd,"%Y-%m-%d").replace("-",""))
        ax.xaxis.tick_top()
        plt.savefig(svpath + "//heatmap_" + str(cow) + "_" + str(dd).replace("-",""))
        plt.close()       
        

#------------------combine heatmap with budget-area over time------------------
#barn limits
barn = {"60":[64.8,75.6],
        "61":[54,64.8],
        "62":[32.4,54],
        "70":[21.6,32.4],
        "71":[10.8,21.6],
        "72":[0,10.8],
        "73":[-10.8,0],
        }
palette = {"walking" : "darkorange",
           "concentrate " : "tan",
           "cubicle_A" : "darkorchid",
           "cubicle_B" : "violet",
           "cubicle_C" : "palevioletred",
           "feed_rack" : "crimson",
           "drink_trough" : "gold",
           "slatted" : "limegreen",
           "resting" : "darkorchid",
           "waiting_area" : "mediumseagreen",
           "unknown" : "royalblue"}

cows = data["cowid"].drop_duplicates().reset_index(drop=1)
for cow in cows:
    print(cow)
    days = data.loc[(data["cowid"]==cow),["date"]].drop_duplicates() 
    days = days.sort_values(by = ["date"]).reset_index(drop=1)
    
    for dd in days["date"]:
        print(dd)
        subset = data.loc[(data["cowid"]==cow) & \
                          (data["date"]==dd),:].copy()
        # set limits (based on information barn)
        barnnr = int(data.loc[(data["cowid"] == cow) &
                          (data["date"] == dd),"barn"].mean())
        x_lim = barn[str(barnnr)]
        y_lim = [-18,0]
        
        fig, ax = plt.subplots(1, 2, gridspec_kw={'width_ratios': [3, 1.5]}, figsize = (22,7))
        # plot scatter + line
        sns.scatterplot(data=subset,x="t",y="zone",
                        hue="area",
                        marker = "s", linewidth = 0,
                        palette = palette,
                        legend = False,
                        ax = ax[0])
        sns.lineplot(data=subset, x='t',y = 'zone', 
                     linewidth = 0.2, color = 'k', ax = ax[0])
        ax[0].set_yticks(range(9))
        ax[0].set_yticklabels(["walking","cubicles","feed rack","drink trough",
                               "concentrate feeder","slatted (b62)","resting (b62)",
                               "waiting area","unknown"])
        sns.set_style("ticks")
        ax[0].set_xticks([0,14400,28800,43200,57600,72000,86400])
        ax[0].set_xticklabels(['00:00','04:00','08:00','12:00','16:00','20:00','24:00'])
        ax[0].set_xlabel("time [h]")
        ax[0].set_title("barn = " +str(barnnr) + ', cow = ' + str(cow) + \
                     ", date = " + str(dd))
        ax[0].set_xlim([-1000,87400])
        
        # plot heatmap
        ax[1] = heatmap_barn(subset["xnew"], subset["ynew"], 0.1, 0.1, x_lim, y_lim)
        ax[1].set_xlabel('y coordinate')
        ax[1].set_ylabel('x coordinate')
        ax[1].set_title("barn = " +str(barnnr) + ', cow = ' + str(cow) + \
                     ", date = " + str(dd))
        ax[1].xaxis.tick_top()
        ax[1].yaxis.tick_right()
        ax[1].yaxis.set_label_position("right")
        # ax[1].invert_xaxis()
        
        # plot rectangles ~ barn areas
        myarea = area_zones.loc[(area_zones["barn"] == barnnr) | \
                            (area_zones["barn"].isna() == True) \
                            ,:].reset_index(drop=1)
        myarea = myarea.drop((myarea.loc[myarea["zone"]==7]).index)
        myarea = myarea.drop((myarea.loc[myarea["zone"]==0]).index)
        myarea = myarea.reset_index(drop=1)
        yax = ax[1].get_ylim()
        ycor = barn[str(barnnr)]
        colors = []
        for j in range(0,len(myarea)):
            x1,y1,x2,y2,x3,y3,x4,y4 = myarea[["x1","y1","x2","y2","x3","y3","x4","y4"]].values[j]
            color = myarea["area"][j]
            X = y3*(-10)
            W = (y2*(-10)) - X
            Y = round((x1-ycor[0]),2)*10
            H = round((x4-x3),2)*10
            ax[1].add_patch(Rectangle((X,Y),W,H, color = palette[color],
                                    fc = palette[color],
                                    alpha = 0.15,
                                    linestyle = 'dotted',
                                    linewidth= 2
                                    ))
            ax[1].add_patch(Rectangle((X,Y),W,H, color = palette[color],
                                    fc = 'none',
                                    linestyle = 'dotted',
                                    linewidth= 2
                                    ))
        del x2,y2,x3,y3,x4,y4,X,Y,H,W

        plt.savefig(svpath+"\\spatial_cow_" + str(cow) + "_" + str(dd).replace("-","") + ".png")
        plt.close()
            
   
#%% exploration and visualisation of behaviours
"""
TODO:
    - longitudinal at group level - per barn
    - longitudinal at group level - all barns
    - longitudinal at individual level - barn 70 and 72
    

"""
import os
import pandas as pd
#import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
# %matplotlib qt

os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research" +
         r"\iAdriaens_doc\Projects\cKamphuis\nlas\scripts\uwb") 


# load data activity
path = os.path.join("W:","/ASG","WLR_Dataopslag","DairyCampus",
                    "3406_Nlas","uwb_processed")

# savepath
svpath = os.path.join("C:","/Users","adria036","OneDrive - Wageningen University & Research",
                    "iAdriaens_doc","Projects","cKamphuis","nlas","results","exploration")

# read activity
fn = [f for f in os.listdir(path) if "activity" in f]
act = pd.DataFrame([])
for f in fn:
    print(f)
    # read data
    new = pd.read_csv(os.path.join(path,f),index_col=0)
    act = pd.concat([act,new])
del new

# read bouts
fn = [f for f in os.listdir(path) if "bouts" in f]
bouts = pd.DataFrame([])
for f in fn:
    print(f)
    # read data
    new = pd.read_csv(os.path.join(path,f),index_col=0)
    bouts = pd.concat([bouts,new])
del new

# read summaries per day
fn = [f for f in os.listdir(path) if ("summary" in f)]
bsum = pd.DataFrame([])
names = ["cowid","barn","date","behaviour","len1_av","len2_av","no_bouts","gap_med", "total1", "total2","nmeas","perc" ]
for f in fn:
    print(f)
    # read data
    new = pd.read_csv(os.path.join(path,f), header = None, 
                      index_col = None, skiprows = 1, names =  names  )
    bsum = pd.concat([bsum,new])
del new
del f,fn

# dtypes
act["date"] = pd.to_datetime(act["date"],format = "%Y-%m-%d")
bouts["date"] = pd.to_datetime(bouts["date"],format = "%Y-%m-%d")
bouts["btime"] = pd.to_datetime(bouts["date"],format = "%Y-%m-%d %H:%M:%s")
bouts["etime"] = pd.to_datetime(bouts["date"],format = "%Y-%m-%d %H:%M:%s")
bsum["date"] = pd.to_datetime(bsum["date"],format = "%Y-%m-%d")

# sort
bouts = bouts.sort_values(by = ["behaviour","date","barn","cowid"])
bsum = bsum.sort_values(by = ["behaviour","date","barn","cowid"]).reset_index(drop=1)
bsum = bsum.drop_duplicates()

# read cowids
cowids = pd.read_csv(os.path.join(path,"cowid_alias.txt"),index_col = None)
cowids = cowids.drop_duplicates()
cowids["at"] = pd.to_datetime(cowids["at"],format = "%Y-%m-%d %H:%M:%S.%f")

#%% visualise with boxplots at group level over all barns and dates

for behaviour in ["feed","drink","cubicle","concentrate","wait","resting","slatted"]:
    # calculate average
    # averbeh = bsum[["date","behaviour","no_bouts","perc"]].groupby(by = ["date","behaviour"]).mean().reset_index()
    # anicount = bsum[["date","behaviour","no_bouts","perc"]].groupby(by = ["date","behaviour"]).count().reset_index()
    subset = bsum.loc[bsum["behaviour"] == behaviour,:].reset_index(drop=1)
    
    custom_params = {"axes.spines.right": True, "axes.spines.top": True,
                     "axes.spines.left": True,"axes.spines.bottom": True,
                     "axes.edgecolor": 'slategrey'
                    }
    sns.set_theme(style = "darkgrid", rc = custom_params, font_scale = 1.5)
    fig,ax = plt.subplots(nrows=3,ncols=1,figsize = (25,13),sharex = True)
    sns.lineplot(data=subset,
                 x="date",y="no_bouts",
                 linewidth = 3, color = 'darkblue',
                 estimator = "mean", errorbar = "sd",err_style = "band",
                 legend=False, ax = ax[0])
    sns.lineplot(data=subset,
                 x="date",y="perc",
                 linewidth = 3, color = 'purple',
                 estimator = "mean", errorbar = "sd",err_style = "band",
                 legend=False, ax = ax[1])
    sns.lineplot(data=subset,
                 x="date",y="total2",
                 linewidth = 3, color = 'mediumseagreen',
                 estimator = "mean", errorbar = "sd",err_style = "band",
                 legend=False, ax = ax[2])
    ax[0].set_title("Overview of daily " + behaviour + " behaviour for all barns and cows")
    ax[2].set_xlabel("date")
    ax[0].set_ylabel("no. of bouts")
    ax[1].set_ylabel("percentage of time [%]")
    ax[2].set_ylabel("total duration [min]")
    ax[1].set_xlim([subset["date"].min(),subset["date"].max()])
    ax[1].set_ylim([0,ax[1].get_ylim()[1]])
    ax[0].set_ylim([0,ax[0].get_ylim()[1]])
    ax[2].set_ylim([0,ax[2].get_ylim()[1]])
    sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-6"),pd.to_datetime("2022-9-6")],
                                      'y' : [0,ax[1].get_ylim()[1]]}),
                 x='x',y='y',estimator=None,
                 color  = "red",lw = 2.5, ax = ax[1])
    sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-6"),pd.to_datetime("2022-9-6")],
                                      'y' : [0,ax[0].get_ylim()[1]]}),
                 x='x',y='y',estimator=None,
                 color  = "red",lw = 2.5, ax = ax[0])
    sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-6"),pd.to_datetime("2022-9-6")],
                                      'y' : [0,ax[2].get_ylim()[1]]}),
                 x='x',y='y',estimator=None,
                 color  = "red",lw = 2.5, ax = ax[2])
    sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-28"),pd.to_datetime("2022-9-28")],
                                      'y' : [0,ax[1].get_ylim()[1]]}),
                 x='x',y='y',estimator=None,
                 color  = "gold",lw = 2.5, ax = ax[1])
    sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-28"),pd.to_datetime("2022-9-28")],
                                      'y' : [0,ax[0].get_ylim()[1]]}),
                 x='x',y='y',estimator=None,
                 color  = "gold",lw = 2.5, ax = ax[0])
    sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-28"),pd.to_datetime("2022-9-28")],
                                      'y' : [0,ax[2].get_ylim()[1]]}),
                 x='x',y='y',estimator=None,
                 color  = "gold",lw = 2.5, ax = ax[2])
    plt.savefig(os.path.join(svpath,"corr_allbarns_" + behaviour + ".png"))
    #plt.close()
    
#%% plots per barn

for behaviour in ["feed","drink","cubicle","concentrate","wait","resting","slatted"]:
    for barn in bsum["barn"].drop_duplicates().reset_index(drop=1):
        #print(barn)
        subset = bsum.loc[(bsum["behaviour"] == behaviour) & \
                          (bsum["barn"] == barn),:]
        if len(subset) > 0:
        
            custom_params = {"axes.spines.right": True, "axes.spines.top": True,
                             "axes.spines.left": True,"axes.spines.bottom": True,
                             "axes.edgecolor": 'slategrey'
                            }
            sns.set_theme(style = "darkgrid", rc = custom_params, font_scale = 1.5)
            fig,ax = plt.subplots(nrows=3,ncols=1,figsize = (25,13),sharex = True)
            sns.lineplot(data=subset.reset_index(drop=1),
                         x="date",y="no_bouts",
                         linewidth = 3, color = 'darkblue',
                         estimator = "median", errorbar = "ci",err_style = "band",
                         legend=False, ax = ax[0])
            sns.lineplot(data=subset,
                         x="date",y="perc",
                         linewidth = 3, color = 'purple',
                         estimator = "median", errorbar = "ci",err_style = "band",
                         legend=False, ax = ax[1])
            sns.lineplot(data=subset,
                         x="date",y="total2",
                         linewidth = 3, color = 'mediumseagreen',
                         estimator = "median", errorbar = "ci",err_style = "band",
                         legend=False, ax = ax[2])
            ax[0].set_title("Daily " + behaviour + " behaviour in barn " + str(barn))
            ax[2].set_xlabel("date")
            ax[0].set_ylabel("no. of bouts")
            ax[1].set_ylabel("percentage of time [%]")
            ax[2].set_ylabel("total duration [min]")
            ax[1].set_xlim([subset["date"].min(),subset["date"].max()])
            ax[1].set_ylim([0,ax[1].get_ylim()[1]])
            ax[0].set_ylim([0,ax[0].get_ylim()[1]])
            ax[2].set_ylim([0,ax[2].get_ylim()[1]])
            sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-6"),pd.to_datetime("2022-9-6")],
                                              'y' : [0,ax[1].get_ylim()[1]]}),
                         x='x',y='y',estimator=None,
                         color  = "red",lw = 2.5, ax = ax[1])
            sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-6"),pd.to_datetime("2022-9-6")],
                                              'y' : [0,ax[0].get_ylim()[1]]}),
                         x='x',y='y',estimator=None,
                         color  = "red",lw = 2.5, ax = ax[0])
            sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-6"),pd.to_datetime("2022-9-6")],
                                              'y' : [0,ax[2].get_ylim()[1]]}),
                         x='x',y='y',estimator=None,
                         color  = "red",lw = 2.5, ax = ax[2])
            sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-28"),pd.to_datetime("2022-9-28")],
                                              'y' : [0,ax[1].get_ylim()[1]]}),
                         x='x',y='y',estimator=None,
                         color  = "gold",lw = 2.5, ax = ax[1])
            sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-28"),pd.to_datetime("2022-9-28")],
                                              'y' : [0,ax[0].get_ylim()[1]]}),
                         x='x',y='y',estimator=None,
                         color  = "gold",lw = 2.5, ax = ax[0])
            sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-28"),pd.to_datetime("2022-9-28")],
                                              'y' : [0,ax[2].get_ylim()[1]]}),
                         x='x',y='y',estimator=None,
                         color  = "gold",lw = 2.5, ax = ax[2])
            plt.savefig(os.path.join(svpath,"corr_barns_" + str(barn) + "_" + behaviour + ".png"))
            plt.close()

#%% all barns with same behaviour in the same plot  (% time)


for behaviour in ["feed","drink","cubicle","concentrate","wait","resting","slatted"]:
   
    subset = bsum.loc[(bsum["behaviour"] == behaviour) & \
                      (bsum["date"] >= pd.to_datetime("2022-9-6")),:]
    fig,ax = plt.subplots(nrows=len(subset["barn"].drop_duplicates()),ncols=1,
                          figsize = (20,3*len(subset["barn"].drop_duplicates())), 
                          sharex=True)
    T=-1
    for barn in subset["barn"].drop_duplicates().sort_values().reset_index(drop=1):
        T = T+1
        custom_params = {"axes.spines.right": True, "axes.spines.top": True,
                     "axes.spines.left": True,"axes.spines.bottom": True,
                     "axes.edgecolor": 'slategrey'
                    }
        sns.set_theme(style = "darkgrid", rc = custom_params, font_scale = 1.5)
        sns.lineplot(data=subset.loc[subset["barn"]==barn],
                 x="date",y="perc",
                 linewidth = 3, color = "purple",
                 estimator = "mean",
                 #errorbar = "ci",err_style = "band",
                 legend=False, ax = ax[T])
        
        ax[T].set_title("barn " +str(barn))
        
        ax[T].set_ylabel("[%]")
        ax[T].set_xlim([subset["date"].min(),subset["date"].max()])
        ax[T].set_ylim([0,subset["perc"].max()+0.1*subset["perc"].max()])
        sns.lineplot(data = pd.DataFrame({'x' : [pd.to_datetime("2022-9-28"),pd.to_datetime("2022-9-28")],
                                          'y' : [0,ax[T].get_ylim()[1]]}),
                     x='x',y='y',estimator=None,
                     color  = "gold",lw = 2.5, ax = ax[T])
    ax[T].set_xlabel("date")
    ax[0].set_title(behaviour + " behaviour, barn " + str(subset["barn"].drop_duplicates().sort_values().reset_index(drop=1)[0]))
     
    
    
    
    # plt.savefig(os.path.join(svpath,"allbarns_secondperiod_" + behaviour + ".png"))
    # plt.close()

#%% visualisation of missing data "unknown" (not interpolated)

# over all cows and all days
# fig,ax = plt.subplots(nrows=1,ncols=1,
#                       figsize = (20,6))
# subset = bsum.loc[(bsum["behaviour"]=="unknown") & \
#                   (bsum["date"] >= pd.to_datetime("2022-10-1")),
#                   ["cowid","barn","perc"]].groupby(by = ["barn","cowid"]).mean().reset_index()
# subset2 = bsum.loc[(bsum["behaviour"]=="unknown") & \
#                   (bsum["date"] >= pd.to_datetime("2022-10-1")),
#                   ["cowid","barn","perc"]].groupby(by = ["barn","cowid"]).std().reset_index()
# subset["std"] = subset2["perc"]

# subset["cowid2"] = subset["cowid"].astype(str)  
                                                       
# sns.barplot(data = subset,
#             x = 'cowid2', y = 'perc', hue = 'barn',
#             errorbar = "sd",palette = "dark")



for barn in bsum["barn"].drop_duplicates():
    subset = bsum.loc[(bsum["behaviour"]=="unknown") & \
                      (bsum["date"] >= pd.to_datetime("2022-10-1")) & \
                      (bsum["barn"] == barn),
                      ["cowid","barn","perc"]]
    subset["cowid2"] = subset["cowid"].astype(str) 
    subset = subset.sort_values(by = "barn") 
    fig,ax = plt.subplots(nrows=1,ncols=1,
                          figsize = (20,6))  
    order = subset[["cowid2","perc"]].groupby(by="cowid2").mean().sort_values(by = "perc").reset_index()   
    order2 = subset[["cowid2","perc"]].groupby(by="cowid2").count().reset_index()    
    order = pd.merge(order,order2, on = "cowid2")
    order = order.rename(columns = {"perc_y" : "counts", "perc_x" : "mean"})                                   
    sns.barplot(data = subset,
                x = 'cowid2', y = 'perc',
                order = order.cowid2,
                estimator = "mean",errorbar = "se",
                palette = "flare", errcolor = ".5"
                )
    ax.set_title("barn " + str(barn))
    
    pos = range(len(order))
    position = 0.01*ax.get_ylim()[1]
    for tick,label in zip(pos,ax.get_xticklabels()):
        ax.text(pos[tick],position, order["counts"][tick], horizontalalignment='center', size='x-small', color='w', weight='semibold')



#%% make bar plots per date : how many cows are in the barn

# datelim = pd.DataFrame({'startdate' : [pd.to_datetime(), pd.to_datetime() ] })


for barn in [60,61,62,70,72,73]:
    subset = bsum.loc[(bsum["behaviour"]=="feed") & \
                      (bsum["date"] >= pd.to_datetime("2022-9-1")) & \
                      (bsum["barn"] == barn),
                      ["date","cowid","barn","perc"]]
    subset["count"] = 1
    # subset["cowid2"] = subset["cowid"].astype(str) 
    # subset = subset.sort_values(by = "barn")
    custom_params = {"axes.spines.right": True, "axes.spines.top": True,
                     "axes.spines.left": True,"axes.spines.bottom": True,
                     "axes.edgecolor": 'slategrey'
                    }
    sns.set_theme(style = "darkgrid", rc = custom_params, font_scale = 1.5)
    fig,ax = plt.subplots(nrows=1,ncols=1,
                          figsize = (20,8))  
    # order = subset[["cowid2","perc"]].groupby(by="cowid2").mean().sort_values(by = "perc").reset_index()   
    # order2 = subset[["cowid2","perc"]].groupby(by="cowid2").count().reset_index()    
    # order = pd.merge(order,order2, on = "cowid2")
    # order = order.rename(columns = {"perc_y" : "counts", "perc_x" : "mean"})                                   
    sns.barplot(data = subset,
                x = 'date', y = 'count',
                #order = order.cowid2,
                estimator = "sum",
                palette = "flare",
                ax = ax
                )
    ax.set_title("number of cows in barn " + str(barn))
    xticklabels = ax.get_xticklabels()
    xticklabels = xticklabels[1::25]
    xticklabels=[item.get_text()[:10] for item in xticklabels]
    xticks = ax.get_xticks()
    xticks = xticks[1::25]
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)
    
    # pos = range(len(order))
    # position = 0.01*ax.get_ylim()[1]
    # for tick,label in zip(pos,ax.get_xticklabels()):
    #     ax.text(pos[tick],position, order["counts"][tick], horizontalalignment='center', size='xx-small', color='w', weight='semibold')

    # subset = bsum.loc[(bsum["behaviour"]=="feed") & \
    #                   (bsum["date"] >= pd.to_datetime("2022-9-1")) & \
    #                   (bsum["barn"] == 62),
    #                   ["date","cowid","barn","perc"]]
    # subset["count"] = 1
    # sns.barplot(data = subset,
    #             x = 'date', y = 'count',
    #             #order = order.cowid2,
    #             estimator = "sum",
    #             palette = "flare",
    #             ax = ax[1]
    #             )
    # ax[1].set_title("number of cows in barn " + str(62))
    plt.savefig(os.path.join(svpath,"number_of_cows_barn" + str(barn) + ".png"))
    #plt.close()



#%% cowids and alias

cowids = cowids.sort_values(by=["cowid","at"]).reset_index(drop=1)


