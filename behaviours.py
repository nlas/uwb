# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 16:47:19 2023

@author: adria036
"""


import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\nlas\scripts\uwb") 


#%% import modules

from datetime import date, datetime
import pandas as pd
import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib
# import seaborn as sns
from uwbfunctions import behaviour_bouts
#%matplotlib qt


#%% import data, seth paths and constants


# path_out 
svpath = r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\nlas\results\exploration"

# path to per barn areas
path = os.path.join("W:","\ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","uwb_processed")
path_zones = os.path.join("W:","\ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_sewio")
fna = "\\Barn_areas.xlsx"
area_zones = pd.read_excel(path_zones+fna, sheet_name = "areas")    
# barn_edges = pd.read_excel(path_zones+fna, sheet_name = "edges")
del fna, path_zones


# settings
settings = {'barn' : [70], # [60,61,62,70,71,72,73],
            'startdate' : date(2022,1,1),
            'enddate' :  date(2022,12,31),
            'cows' : [464], # or specific cow number
            }

# files that comply with settings
fn = []
for b in range(0,len(settings["barn"])):
    print("barn = " + str(settings["barn"][b]))
    if settings["cows"] == 0:
        fbarn = [f for f in os.listdir(path + "/barn" + str(settings["barn"][b])) \
                if os.path.isfile(os.path.join(path,"barn"+str(settings["barn"][b]),f)) \
                    and (datetime.strptime(f[5:13], '%Y%m%d').date() >= settings["startdate"]) \
                    and (datetime.strptime(f[5:13], '%Y%m%d').date() <= settings["enddate"])]
        fbarn.sort()
    else:
        fbarn = [f for f in os.listdir(path + "/barn" + str(settings["barn"][b])) \
                if os.path.isfile(os.path.join(path,"barn"+str(settings["barn"][b]),f)) \
                    and (int(f[26:-4]) in settings["cows"]) \
                    and (datetime.strptime(f[5:13], '%Y%m%d').date() >= settings["startdate"]) \
                    and (datetime.strptime(f[5:13], '%Y%m%d').date() <= settings["enddate"])]
        fbarn.sort()
    fn.extend(fbarn)
    fn.sort()

# find unique cows
cows = list(set([int(f[26:-4]) for f in fn]))

# read data
data = pd.DataFrame([])
for f in fn:
    barn = f[19:21]
    sub = pd.read_csv(path + "/barn" + barn + "/" + f, 
                      usecols = ["cowid","barn","date","t","xnew","ynew","area","zone","X","y"],
                      dtype = {"cowid" : "int64","barn" : "int64","date" : "object",
                               "t" : "int64", "xnew":"float64","ynew":"float64",
                               "area":"object","zone":"float64","X":"float64","y" : "float64"})
    sub["date"] = pd.to_datetime(sub["date"], format = "%Y-%m-%d") 
    data = pd.concat([data,sub])
data = data.sort_values(by = ["cowid","date","t"])
data = data.reset_index(drop=1)



data.loc[data["zone"].isna(),"zone"] = 8
data.loc[data["zone"] == 8 ,"area"] = "unknown"

data["date"] = data["date"].dt.date

# check correctly read ids
print(data[["cowid","barn"]].drop_duplicates())



#%% calculate and summarize behaviours


cows = data["cowid"].drop_duplicates().reset_index(drop=1)
bouts = pd.DataFrame([])
bsums = pd.DataFrame([])
act_dist = pd.DataFrame([])
for cow in cows:
    days = data.loc[(data["cowid"]==cow),["date"]].drop_duplicates() 
    days = days.sort_values(by = ["date"]).reset_index(drop=1)
    
    for dd in days["date"]:
        print("cow = " + str(cow) + ", date = " + str(dd))
        dset = data.loc[(data["cowid"] == cow) & (data["date"] == dd)].copy()
        # ------------------------ feeding behaviour --------------------------
        interval = 5*60      # min number of seconds between separate bouts
        behaviour = "feed"   # feeding behaviour
        min_length = 30      # do not count if shorter than 30 seconds
        df,summary = behaviour_bouts(dset, interval, behaviour,min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # ---------------------- drinking behaviour ---------------------------
        interval = 5*60      # min number of seconds between separate bouts
        behaviour = "drink"   # drinking behaviour
        min_length = 5      # do not count if shorter than 2 seconds
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # ---------------------- resting behaviour ----------------------------
        interval = 2*60      # min number of seconds between separate bouts
        behaviour = "cubicle"   # resting behaviour
        min_length = 30      # do not count if shorter than 30 seconds
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # -------------------------- cubicle_A --------------------------------
        interval = 2*60      # min number of seconds between separate bouts
        behaviour = "cubicle_A"   # resting behaviour
        min_length = 30      # do not count if shorter than 30 seconds
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # -------------------------- cubicle_B --------------------------------
        interval = 2*60      # min number of seconds between separate bouts
        behaviour = "cubicle_B"   # resting behaviour
        min_length = 30      # do not count if shorter than 30 seconds
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # -------------------------- cublicle_C -------------------------------
        interval = 2*60      # min number of seconds between separate bouts
        behaviour = "cubicle_C"   # resting behaviour
        min_length = 30      # do not count if shorter than 30 seconds
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # ------------------------ concentrate feeder -------------------------
        interval = 5*60      # min number of seconds between separate bouts
        behaviour = "concentrate"   # concentrate feeding behaviour
        min_length = 2       # always count
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # ------------------------ waiting area ------------------------------
        interval = 20*60      # min number of seconds between separate bouts
        behaviour = "wait"   # in the waiting area
        min_length = 0        # always count
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # ------------------------ unknown area ------------------------------
        interval = 5*60      # min number of seconds between separate bouts
        behaviour = "unknown"   # unknown behaviour
        min_length = 10      # do not count if shorter than 10 seconds
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # ------------------------- slatted (barn 62) -------------------------
        interval = 5*60      # min number of seconds between separate bouts
        behaviour = "slatted"   # unknown behaviour
        min_length = 10      # do not count if shorter than 10 seconds
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # ------------------------- resting (barn 62) -------------------------
        interval = 5*60      # min number of seconds between separate bouts
        behaviour = "resting"   # unknown behaviour
        min_length = 10      # do not count if shorter than 10 seconds
        df,summary = behaviour_bouts(dset, interval, behaviour, min_length)
        bouts = pd.concat([bouts,df],axis = 0).reset_index(drop=1)
        bsums = pd.concat([bsums,summary],axis = 0).reset_index(drop=1)
        
        # # ------------------------- distance travelled ------------------------
        dset.loc[:,"dist"] = 0
        dset.iloc[1:,dset.columns == "dist"] = \
                np.sqrt((dset["xnew"].iloc[:-1].values-dset["xnew"].iloc[1:].values)**2 + \
                        (dset["ynew"].iloc[:-1].values-dset["ynew"].iloc[1:].values)**2)
        dset.loc[(dset["dist"]<0.30) | (dset["dist"]>5),"dist"] = 0
        
        # distance travelled and % active (with and without nans)
        act = dset[["cowid","date","barn","dist"]].groupby(by = ["cowid","date","barn"]).sum().reset_index()
        act["perc_act"] = round(len(dset.loc[dset["dist"] > 0])/len(dset)*100,2)
        act["perc_act_nan"] = round(len(dset.loc[dset["dist"] > 0])/len(dset.loc[dset["area"]!="unknown"])*100,2)
        
        # % of the day not in the cubicles
        act["perc_no_lying"] = 100- round(len(dset.loc[(dset["area"].str.contains("cubicle") == True) | \
                                             (dset["area"].str.contains("resting") == True)])/len(dset)*100,2)
        act["perc_no_lying_nan"] = 100- round(len(dset.loc[(dset["area"].str.contains("cubicle") == True) | \
                                             (dset["area"].str.contains("resting") == True)])/len(dset.loc[dset["area"]!="unknown"])*100,2)
        
        act_dist = pd.concat([act_dist,act]).reset_index(drop=1)
