# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 08:52:20 2022
@author: adria036
Project: NLAS-innovation
Contributors: Ines Adriaens, Bert Klandermans, Tomas Izquierdo
Contact: ines.adriaens@wur.nl

Set of functions to read sewio data from data base
-------------------------------------------------------------------------------
This script contains the functions to read sewio data from parquet files.
You can specify which time frame to consider.

Contains:
    - start_end_dates
    - read_parquet_accel
    - convert_accel_data
    - read_parquet_pos
    - read_feeds
    - read_all_data

Packages you need to import:
    import pandas as pd
    import os
    from datetime import date, timedelta, datetime

-------------------------------------------------------------------------------
Usage example:
    # import packages
    from datetime import  date,timedelta
    from read_data_sewio import *

    # set parameters
    check_date = date.today() - timedelta(days=1)
    comparison_window = 1 # set to -1 to only load one day
    path =  r"W:/ASG/WLR_Dataopslag/DairyCampus/3406_Nlas/raw/dc_sewio/"

    # start and end date
    startdate,enddate = start_end_dates(check_date,comparison_window)

    # read data all
    data = read_all_data(startdate,enddate,path)

----------------------------------------------------------------------------
Data source: W:/ drive -- or DB copy

"""

#%% import packages
import pandas as pd
import os
from datetime import timedelta, datetime
import copy
import fastparquet
import openpyxl

#%% define functions

# function to define the window which data to read
def start_end_dates(check_date,comparison_window):
    """
    Description
    -----------
    Gives the end and start date with the window you need when comparing for
    QC

    Parameters
    ----------
    check_date : TYPE = datetime
        for QC: date you want data to benchmark from.
    comparison_window : TYPE = int
        benchmark window.

    Returns
    -------
    startdate : TYPE = datetime
        startdate of data to consider.
    enddate : TYPE = dateime
        enddate of data to consider.

    """
    # start date
    startdate = check_date - timedelta(days = comparison_window)
    enddate = check_date
    return startdate, enddate

# function to read accelerometer data
def read_parquet_accel(startdate,enddate,path):
    """
    Description
    -----------
    Reads accelerometer data from the parquet files stores in W: drive

    Parameters
    ----------
    startdate : TYPE = datetime
        date of first file you want to read
    enddate : TYPE = datetime
        date of last file you want to read
    path : TYPE = str
        path where files are stored

    Returns
    -------
    data : TYPE = pandas DataFrame
        DataFrame with the read parquet files of acceleration data
        col = ['history_id','date','at','acc_x','acc_y','acc_z']

    """
    # check file path
    if os.path.isdir(path):
        # set filenames based on daterange (.parquet)
        fn = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path,f)) \
                and ".parquet" in f \
                and datetime.strptime(f[0:10], '%Y-%m-%d').date() >= startdate \
                and datetime.strptime(f[0:10], '%Y-%m-%d').date() <= enddate]
        fn.sort()
        # read data
        data = pd.DataFrame([])
        for f in range(0,len(fn)):
            print(fn[f])
            accdata = pd.read_parquet(os.path.join(path,fn[f]), engine='auto')
            # cast to datetime to reduce computing memory
            accdata['at'] = pd.to_datetime(accdata['at'] )
            accdata["at"] = accdata["at"].dt.tz_localize('UCT')
            accdata["at"] = accdata["at"].dt.tz_convert('CET')
            accdata['date'] = pd.to_datetime(accdata['date'] )
            accdata["date"] = accdata["date"].dt.tz_localize('UCT')
            accdata["date"] = accdata["date"].dt.tz_convert('CET')
            
            # added
            if not 'history_id' in accdata.columns:
                accdata['history_id'] = accdata.index

            # cast to numeric
            accdata['history_id'] = pd.to_numeric(accdata['history_id'],errors='coerce')

            #print(fn[f], accdata.memory_usage(deep=True))

            # extract values (strings) to temporary dataframe
            value = accdata.value

            # drop useless columns
            accdata = accdata.drop(axis = 1,
                                   columns = ['value'])
            value = value.str.split(pat=";",expand=True) # split string + to int
            value = value.rename(columns={0:"acc_x",1:"acc_y",2:"acc_z"}) # rename

            # set 'N/A' to 'NaN' for type convertion to float if str
            try:
                value.loc[value["acc_x"].str.contains('N/A'),"acc_x"] = 'NaN'
            except:
                pass
            try:
                value.loc[value["acc_y"].str.contains('N/A'),"acc_y"] = 'NaN'
            except:
                pass
            try:
                value.loc[value["acc_z"].str.contains('N/A'),"acc_z"] = 'NaN'
            except:
                pass
            # drop rows with '%' values in x_acceleration
            value = value.drop(value[(value["acc_x"].str.contains("%")==True)].index)

            # cast to numeric
            value['acc_x'] = pd.to_numeric(value['acc_x'],errors='coerce')
            value['acc_y'] = pd.to_numeric(value['acc_y'],errors='coerce')
            value['acc_z'] = pd.to_numeric(value['acc_z'],errors='coerce')
            accdata['acc_x'] = value['acc_x']
            accdata['acc_y'] = value['acc_y']
            accdata['acc_z'] = value['acc_z']
            del value

            data = pd.concat([data,accdata],axis=0, ignore_index=1)
            
            #print(fn[f], data.memory_usage(deep=True))
    return data

# function to convert accelerometer data
def convert_accel_data(a_raw,dynamic_range = 2,to_ms = True):
    """
    Description
    -----------
    Converts the specified value to G-units, as defined by Noldus manual

    a[ms**2] = (a_raw * dynamic_range * g) / scale_factor

        with
            g = gravitational_acceleration = 9.81 (to convert to ms**2)
            a_raw= raw acceleration as extracted from sewio
            dynamic range = [2,4,8,16] in current sewio sensors
            scale_factor = 2**15

     Parameters
     ----------
     value : TYPE = Series of floats
         Series of raw acceleration values.
     dynamic_range : TYPE = int
         ?? setting of sewio
     to_ms : TYPE = Boolean
         if True: convert to acceleration in meter per second*second
         if False: convert to G-units

     Returns
     -------
     acceleration : TYPE = Series of floats
         Converted acceleration values.   

    """
    # set scale factor (sewio setting) and g
    scale_factor = 2**15
    g = 9.81

    # set optional arguments, if not given = use these
    # dynamic_range
    # to_ms = True

    # calculate converted acceleration in g units
    a_new = a_raw * dynamic_range / scale_factor
    # convert to meter per squared second
    if to_ms == True:
        a_new = a_new * g

    return a_new

# read position data
def read_parquet_pos(startdate,enddate,path):
    """
    Description
    -----------
    Read position data from sewio db files stored in W: as parquet

    Parameters
    ----------
    startdate : TYPE = datetime
        date of first file you want to read
    enddate : TYPE = datetime
        date of last file you want to read
    path : TYPE = str
        path where files are stored

    Returns
    -------
    data : pandas DataFrame
        pandas data frame containing x,y position.

    """
    # check file path
    if os.path.isdir(path):
        # set filenames based on daterange (.parquet)
        fn = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path,f)) \
                and ".parquet" in f \
                and datetime.strptime(f[0:10], '%Y-%m-%d').date() >= startdate \
                and datetime.strptime(f[0:10], '%Y-%m-%d').date() <= enddate]
        fn.sort()
        # read data
        data = pd.DataFrame([])
        for f in range(0,len(fn)):
            print(fn[f])
            sewdata = pd.read_parquet(os.path.join(path,fn[f]))
            sewdata['at'] = pd.to_datetime(sewdata['at'])
            sewdata["at"] = sewdata["at"].dt.tz_localize('UCT')
            sewdata["at"] = sewdata["at"].dt.tz_convert('CET')
            sewdata['date'] = pd.to_datetime(sewdata['date'])
            sewdata["date"] = sewdata["date"].dt.tz_localize('UCT')
            sewdata["date"] = sewdata["date"].dt.tz_convert('CET')
            # added
            if not 'position_id' in sewdata.columns:
                sewdata['position_id'] = sewdata.index
            
            # drop useless columns
            sewdata.drop(axis = 1,
                            columns = ['clr','numberOfAnchors','plan_reference'], inplace=True)

            data = pd.concat([data,sewdata],axis=0, ignore_index=1)

    return data

# function to read "feeds" = tag id data
def read_feeds(startdate,enddate, path_os):
    """
    Description
    -----------
    Reads feed and stream data from sewio db files stored in W: as parquet
    These are needed to check IDs

    Parameters
    ----------
    startdate : TYPE = datetime
        startdate of which feed data to read.
    enddate : TYPE = datetime
        enddate of which feed data to read.

    Returns
    -------
    feeds : TYPE = pandas DataFrame
        feeds = sensor ids to merge with data.
    streams : TYPE = pandas DataFrame
        streams = tags to merge
    streamfeeds : TYPE = pandas DataFrame
        streamfeeds = combined streams and feeds
    """
    # prepare filepath construction
    delta = enddate - startdate
    # read file paths
    for d in range(delta.days + 1):
        print(d)
        day_dataset = startdate + timedelta(days = d)
        yy = day_dataset.year
        mm = day_dataset.month
        dd = day_dataset.day
        if dd < 10 and mm < 10:
            path = os.path.join(path_os,'db',str(yy), f'0{str(mm)}',f'0{str(dd)}')
        elif dd > 9 and mm < 10:
            path = os.path.join(path_os,'db',str(yy), f'0{str(mm)}',f'{str(dd)}')
        elif dd < 10 and mm > 9:
            path = os.path.join(path_os,'db',str(yy), f'{str(mm)}',f'0{str(dd)}')
        else:
            path = os.path.join(path_os,'db',str(yy), str(mm),str(dd))
        # read feeds and check unique
        # fn_feeds = 'feeds.parquet'
        feed_path = os.path.join(path, 'feeds.parquet')
        if os.path.isfile(feed_path):
            feeds_all = pd.read_parquet(feed_path)
            feeds_all = feeds_all.loc[:,['id','alias','title']]
            feeds_all = feeds_all.rename(columns={"id":"feed_reference"})
            if 'feeds' not in locals():
                print('feeds = ' + str(yy)+str(mm)+str(dd))
                feeds = feeds_all
            else:
                print('feeds = ' + str(yy)+str(mm)+str(dd))
                feeds = pd.concat([feeds,feeds_all],
                                  axis=0, ignore_index=1,
                                  join = 'outer',
                                  keys = ['feed_reference','alias','title'])
                feeds = feeds.drop_duplicates()
                feeds = feeds.sort_values(by=['feed_reference']).reset_index(drop=1)
        
        # fn_streams = 'datastreams.parquet'
        stream_path = os.path.join(path, 'datastreams.parquet')
        if os.path.isfile(stream_path):
            streams_all = pd.read_parquet(stream_path)
            streams_all = streams_all.loc[:,['datastream_id','feed_reference','d_id']]
            if 'streams' not in locals():
                streams = streams_all
                print('streams = ' + str(yy)+str(mm)+str(dd))
            else:
                print('streams = ' + str(yy)+str(mm)+str(dd))
                streams = pd.concat([streams,streams_all],
                                  axis=0, ignore_index=1,
                                  join = 'outer',
                                  keys = ['datastream_id','feed_reference','d_id'])
                streams = streams.drop_duplicates()
                streams = streams.sort_values(by=['feed_reference']).reset_index(drop=1)
                streams = streams.loc[streams["d_id"].str.contains('acc')  == True]
        else:
            TEL=1
            while (os.path.isfile(stream_path) == False) and (TEL < 30) :
                if dd > 1:
                    dd = dd - 1
                    print("first case, dd= " + str(dd) + ", mm= " + str(mm))
                    if dd < 10 and mm < 10:
                        path = os.path.join(path_os,'db',str(yy), f'0{str(mm)}',f'0{str(dd)}')
                    elif dd > 9 and mm < 10:
                        path = os.path.join(path_os,'db',str(yy), f'0{str(mm)}',f'{str(dd)}')
                    elif dd < 10 and mm > 9:
                        path = os.path.join(path_os,'db',str(yy), f'{str(mm)}',f'0{str(dd)}')
                    else:
                        path = os.path.join(path_os,'db',str(yy), str(mm),str(dd))
                    stream_path = os.path.join(path, 'datastreams.parquet')
                elif (dd == 1) and (mm > 1):
                    mm = mm - 1
                    dd = 28
                    print("second case, dd= " + str(dd) + ", mm= " + str(mm))
                    if dd < 10 and mm < 10:
                        path = os.path.join(path_os,'db',str(yy), f'0{str(mm)}',f'0{str(dd)}')
                    elif dd > 9 and mm < 10:
                        path = os.path.join(path_os,'db',str(yy), f'0{str(mm)}',f'{str(dd)}')
                    elif dd < 10 and mm > 9:
                        path = os.path.join(path_os,'db',str(yy), f'{str(mm)}',f'0{str(dd)}')
                    else:
                        path = os.path.join(path_os,'db',str(yy), str(mm),str(dd))
                    stream_path = os.path.join(path, 'datastreams.parquet')
                elif (dd == 1) and (mm == 1):
                    mm = 12
                    dd = 28
                    yy = yy-1
                    print("third case, dd= " + str(dd) + ", mm= " + str(mm) + ", yy= " + str(yy))
                    if dd < 10 and mm < 10:
                        path = os.path.join(path_os,'db',str(yy), f'0{str(mm)}',f'0{str(dd)}')
                    elif dd > 9 and mm < 10:
                        path = os.path.join(path_os,'db',str(yy), f'0{str(mm)}',f'{str(dd)}')
                    elif dd < 10 and mm > 9:
                        path = os.path.join(path_os,'db',str(yy), f'{str(mm)}',f'0{str(dd)}')
                    else:
                        path = os.path.join(path_os,'db',str(yy), str(mm),str(dd))
                    stream_path = os.path.join(path, 'datastreams.parquet')
                TEL = TEL+1
            streams_all = pd.read_parquet(stream_path)
            streams_all = streams_all.loc[:,['datastream_id','feed_reference','d_id']]
            if 'streams' not in locals():
                streams = streams_all
                print('streams = ' + str(yy)+str(mm)+str(dd))
            else:
                print('streams = ' + str(yy)+str(mm)+str(dd))
                streams = pd.concat([streams,streams_all],
                                  axis=0, ignore_index=1,
                                  join = 'outer',
                                  keys = ['datastream_id','feed_reference','d_id'])
                streams = streams.drop_duplicates()
                streams = streams.sort_values(by=['feed_reference']).reset_index(drop=1)
                streams = streams.loc[streams["d_id"].str.contains('acc')  == True]
        # both together in the same table with an innerjoin
        if 'feeds' not in locals() or 'streams' not in locals():
            streamfeeds = []
        else:
            streamfeeds = streams.merge(feeds)

    return feeds,streams,streamfeeds

def read_ids_LF(path_data, fn):
    """
    Description
    -----------
    Reads the ids from the animals, manually entered at DC when 
    batteries are charged, and couples them to the data.

    Parameters
    ----------
    path : TYPE = str
        path to xlsx file, read as pandas

    Returns
    -------
    tags : TYPE = pandas DataFrame
        Df with all the id data

    """
    # set path
    # path = os.path.join(
    #   "C:","/Users","adria036","OneDrive - Wageningen University & Research",
    #   "iAdriaens_doc","Projects","cKamphuis","nlas","data","ancillary_data"
    #   )
    # fn = "\Sewio_tags.xlsx"

    # read tagIDs and tags2cow from excel file
    tag2cow = pd.read_excel(os.path.join(path_data,fn), sheet_name = "Tag2Cow low frequency", skiprows = [0,1])
    tagIDs = pd.read_excel(os.path.join(path_data,fn), sheet_name = "Tag_IDs", skiprows = [0,1,2,3])
    tagIDs = tagIDs.rename(columns = {'Sewio_alias' : 'Sewio_tagnr'})
    # combine tag with tag id and inspect
    tags = tag2cow.merge(tagIDs)
    tags.head(20)
    # return tags
    return tags


# combine all functions in a single one
def read_all_data(startdate,enddate,path):
    """
    Description
    -----------
    Uses the above functions to read and (outer) merge all data of position 
    and accelerometers into a single DataFrame

    Parameters
    ----------
    startdate : TYPE = datetime
        Startdate for data read
    enddate : TYPE = datetime
        Enddate for data read
    path : TYPE = str
        path where directories "db/", "position/" and "history/" are - these
        contain the actual data files to read.

    Returns
    -------
    data : TYPE = pandas DataFrame
        Df with all the data, also the data that could not be merged between 
        position and accelerometer.

    """
    # read feeds in [startdate,enddate]
    feeds,streams,streamfeeds = read_feeds(startdate, enddate, path)

    # read+convert accelerometer data from [startdate,enddate]
    path_acc = os.path.join(path, "history")
    accel = read_parquet_accel(startdate, enddate, path_acc)
    accel["acc_x"] = convert_accel_data(accel["acc_x"])
    accel["acc_y"] = convert_accel_data(accel["acc_y"])
    accel["acc_z"] = convert_accel_data(accel["acc_z"])
    
    # delete measurements with no acceleration; or when acc_x is available but no y,z
    accel = accel.loc[accel["acc_y"].isna() == False,:]

    # read position data
    path_pos = os.path.join(path, "position")
    pos = read_parquet_pos(startdate, enddate, path_pos)
    
    # combine position with feeds
    pos = pos.merge(feeds)
    pos = pos.drop(axis = 1,
                   columns = ["position_id"])

    # combine accel with streams
    accel = accel.merge(streamfeeds,
                        left_on = 'datastream_reference',
                        right_on = 'datastream_id')
    accel = accel.drop(axis = 1,
                       columns = ['history_id','datastream_reference',
                                'datastream_id','d_id','alias'])
    # combine accel with pos using date, at, feed_reference, alias, title
    data = accel.merge(pos,
                       how ='outer',
                       on = ["date","at","feed_reference","title"])

    return data


def read_ids(path, fn):
    """
    Description
    -----------
    Reads the ids from the animals, manually entered at DC when 
    batteries are charged, and couples them to the data.

    Parameters
    ----------
    path : TYPE = str
        path to xlsx file, read as pandas

    Returns
    -------
    tags : TYPE = pandas DataFrame
        Df with all the id data

    """
    # set path
    # path = os.path.join(
    #   "C:","/Users","adria036","OneDrive - Wageningen University & Research",
    #   "iAdriaens_doc","Projects","cKamphuis","nlas","data","ancillary_data"
    #   )
    # fn = "\Sewio_tags.xlsx"

    # read tagIDs and tags2cow from excel file
    tag2cow = pd.read_excel(os.path.join(path,fn), sheet_name = "Tag2Cow", skiprows = [0,1])
    tagIDs = pd.read_excel(os.path.join(path,fn), sheet_name = "Tag_IDs", skiprows = [0,1,2,3])
    tagIDs = tagIDs.rename(columns = {'Sewio_alias' : 'Sewio_tagnr'})
    # combine tag with tag id and inspect
    tags = tag2cow.merge(tagIDs)
    tags.head(20)
    # return tags
    return tags

def combine_data_id(data,tags):
    """
    Adds ids and barn number to data, based on "tags". Data can be either 
    position, accel or data (both)

    Parameters
    ----------
    data : pandas DataFrame
        Position, accel or data.
    tags : pandas DataFrame
        Read from tags excel sheet with function read_ids.

    Returns
    -------
    data : pandas DataFrame
        with cowid and barn number added for that session.

    """
    # in data: alias = "Sewio_tagnr" and "Sewio_tagID" corresponds to "title"
    # remark: tested for alias = feed_reference but that seems incorrect
    #   principle: run over tags line per line; 
    #   select the records with tag x between dates
    #   add cow id and barn number to selected data
    # first select the records in "tags" of which enddate > min data["date"]
    # correct for someone stupidly changing col name of the file
    try:
        tags = tags.rename(columns={"datetime_begin":"datetime_start"})
    except:
        pass
    
    # correct for formating dates
        #tags["datetime_start"] = tags.datetime_start.apply(lambda x: x.date())
        #tags["datetime_end"] = tags.datetime_end.apply(lambda x: x.date())
    tags["datetime_start"] = pd.to_datetime(tags["datetime_start"])
    tags["datetime_end"] = pd.to_datetime(tags["datetime_end"])
    #data["date"] = data.date.dt.date
    
    # selection step -- changed time efficacy
    tag_selected = tags.loc[((pd.to_datetime(tags["datetime_start"].dt.date) <= data["date"].max()) & \
                        ((pd.to_datetime(tags["datetime_end"].dt.date) >= data["date"].min()))) | \
                        ((pd.to_datetime(tags["datetime_start"].dt.date) <= data["date"].max()) & \
                         (tags["datetime_end"].isna() == True)),:].reset_index(drop=1)
    tag_selected = tag_selected.sort_values(by = ["cow","datetime_start"])
    # set endtime to today if NaT
    tag_selected.loc[(tag_selected["datetime_end"].isna()==True),
                     "datetime_end"] = pd.Timestamp.today()
    # add records    
    newdata = copy.deepcopy(data)
    newdata["barn"] = 0
    newdata["cowid"] = 0
    # add tag nr based on time and alias
    for i in range(0,len(tag_selected)):
        
        # set data of current line       
        tagstart = tag_selected["datetime_start"].iloc[i]
        # tagstart = pd.DataFrame({'at': [tagstart]})
        # tagstart = pd.to_datetime(tagstart['at'].astype(str))
        # tagstart = tagstart.dt.tz_localize('CET')
        
        #tagstart = tagstart.to_pydatetime()
        #tagstart = tagstart - timedelta(hours = 2)
        tagend = tag_selected["datetime_end"].iloc[i]
        # tagend = pd.DataFrame({'at': [tagend]})
        # tagend = pd.to_datetime(tagend['at'].astype(str))
        # tagend = tagend.dt.tz_localize('CET')
        #tagend = tagend - timedelta(hours = 2)
        tagcow = tag_selected["cow"].iloc[i]
        taggroup = tag_selected["group"].iloc[i]
        tagnr = tag_selected["Sewio_tagnr"].iloc[i]
        # find all measurements in data after tagstart 
        #                               before tagend
        #                               with feed_reference=tagnr
        newdata.loc[(newdata["at"] >= tagstart) \
                  & (newdata["at"] <=  tagend) \
                  & (newdata["alias"] == tagnr),"barn"] = taggroup
        newdata.loc[(newdata["at"] >= tagstart) \
                  & (newdata["at"] <= tagend) \
                  & (newdata["alias"] == tagnr),"cowid"] = tagcow
    newdata = newdata.sort_values(by = ["cowid","at"]).reset_index(drop=1)
    # find records for which probably the id records are missing
    no_ids = newdata.loc[(newdata["barn"] == 0) & (newdata["relsec"] < 7*3600),:]
    idx = no_ids[["alias","feed_reference"]].drop_duplicates()
    no_ids = no_ids.loc[idx.index.values,:].reset_index(drop=1)
    nr_rec = newdata.loc[(newdata["barn"] == 0),["alias","feed_reference"]].groupby(by = ["alias"]).count().reset_index()
    nr_rec = nr_rec.rename(columns = {"feed_reference" : "count"})
    no_ids = pd.merge(no_ids,nr_rec,on = "alias",how = "inner")
    no_ids = no_ids.loc[no_ids["count"] > 1800,:]
    
    return newdata, no_ids




#------------------------------------------------------------------------------
#                             HOURLY DATA
#------------------------------------------------------------------------------

# function to read accelerometer data
def read_parquet_accel_hour(startdate,enddate,path_acc):
    """
    Description
    -----------
    Reads accelerometer data from the parquet files stores in W: drive

    Parameters
    ----------
    startdate : TYPE = datetime
        date of first file you want to read
    enddate : TYPE = datetime
        date of last file you want to read
    path : TYPE = str
        path where files are stored

    Returns
    -------
    data : TYPE = pandas DataFrame
        DataFrame with the read parquet files of acceleration data
        col = ['history_id','date','at','acc_x','acc_y','acc_z']

    """
    # check file path
    if os.path.isdir(path_acc):
        # set filenames based on daterange (.parquet)
        # fn = [f for f in os.listdir(path_acc) if os.path.isfile(os.path.join(path_acc,f)) \
        #         and ".parquet" in f \
        #         and datetime.strptime(f[0:10], '%Y-%m-%d').date() >= startdate \
        #         and datetime.strptime(f[0:10], '%Y-%m-%d').date() <= enddate]
        fn = pd.DataFrame(os.listdir(path_acc),columns = ["fn"])
        fn = fn.sort_values(by = "fn").reset_index(drop=1)
        fn["date"] = pd.to_datetime(fn["fn"].str[0:10], format = '%Y-%m-%d')
        fn = fn.loc[(fn["date"] >= pd.to_datetime(startdate)) & (fn["date"]<=pd.to_datetime(enddate)),:] 
        fn = fn["fn"].to_list()

        # read data
        data = pd.DataFrame([])
        for f in range(0,len(fn)):
            # print(fn[f])
            accdata = pd.read_parquet(os.path.join(path_acc,fn[f]), engine='auto')
            if len(accdata) > 0:
                # cast to datetime to reduce computing memory
                accdata['at'] = pd.to_datetime(accdata['at'] )
                accdata["at"] = accdata["at"].dt.tz_localize('UCT')
                accdata["at"] = accdata["at"].dt.tz_convert('CET')
                accdata['date'] = pd.to_datetime(accdata['date'] )
                accdata["date"] = accdata["date"].dt.tz_localize('UCT')
                accdata["date"] = accdata["date"].dt.tz_convert('CET')
                
                # added
                if not 'history_id' in accdata.columns:
                    accdata['history_id'] = accdata.index
    
                # cast to numeric
                accdata['history_id'] = pd.to_numeric(accdata['history_id'],errors='coerce')
    
                #print(fn[f], accdata.memory_usage(deep=True))
    
                # extract values (strings) to temporary dataframe
                value = accdata.value
    
                # drop useless columns
                accdata = accdata.drop(axis = 1,
                                       columns = ['value'])
                if len(value) > 0:
                    value = value.str.split(pat=";",expand=True) # split string + to int
                    value = value.rename(columns={0:"acc_x",1:"acc_y",2:"acc_z"}) # rename
                else:
                    value = pd.DataFrame([],columns = ["acc_x","acc_y","acc_z"])
                    
                # set 'N/A' to 'NaN' for type convertion to float if str
                try:
                    value.loc[value["acc_x"].str.contains('N/A'),"acc_x"] = 'NaN'
                except:
                    pass
                try:
                    value.loc[value["acc_y"].str.contains('N/A'),"acc_y"] = 'NaN'
                except:
                    pass
                try:
                    value.loc[value["acc_z"].str.contains('N/A'),"acc_z"] = 'NaN'
                except:
                    pass
                # drop rows with '%' values in x_acceleration
                value = value.drop(value[(value["acc_x"].str.contains("%")==True)].index)
                
                # drop none in y and z acceleration
                value = value.drop(value[(value["acc_y"].isnull())].index)
                
                # cast to numeric
                value['acc_x'] = pd.to_numeric(value['acc_x'],errors='coerce')
                value['acc_y'] = pd.to_numeric(value['acc_y'],errors='coerce')
                value['acc_z'] = pd.to_numeric(value['acc_z'],errors='coerce')
                accdata['acc_x'] = value['acc_x']
                accdata['acc_y'] = value['acc_y']
                accdata['acc_z'] = value['acc_z']
                del value
    
                data = pd.concat([data,accdata],axis=0, ignore_index=1)
            
            #print(fn[f], data.memory_usage(deep=True))
            try:
                data = data.sort_values(by = "at").reset_index(drop=1)
            except:
                pass
    return data

def read_parquet_pos_hour(startdate,enddate,path_pos):
    """
    Description
    -----------
    Read position data from sewio db files stored in W: as parquet

    Parameters
    ----------
    startdate : TYPE = datetime
        date of first file you want to read
    enddate : TYPE = datetime
        date of last file you want to read
    path : TYPE = str
        path where files are stored

    Returns
    -------
    data : pandas DataFrame
        pandas data frame containing x,y position.

    """
    # check file path
    if os.path.isdir(path_pos):
        # set filenames based on daterange (.parquet)
        # fn = [f for f in os.listdir(path_pos) if os.path.isfile(os.path.join(path,f)) \
        #         and ".parquet" in f \
        #         and datetime.strptime(f[0:10], '%Y-%m-%d').date() >= startdate \
        #         and datetime.strptime(f[0:10], '%Y-%m-%d').date() <= enddate]
        # fn.sort()
        fn = pd.DataFrame(os.listdir(path_pos),columns = ["fn"])
        fn = fn.sort_values(by = "fn").reset_index(drop=1)
        fn["date"] = pd.to_datetime(fn["fn"].str[0:10], format = '%Y-%m-%d')
        fn = fn.loc[(fn["date"] >= pd.to_datetime(startdate)) & (fn["date"]<=pd.to_datetime(enddate)),:] 
        fn = fn["fn"].to_list()
        
        # read data
        data = pd.DataFrame([])
        for f in range(0,len(fn)):
            # print(fn[f])
            sewdata = pd.read_parquet(os.path.join(path_pos,fn[f]))
            if len(sewdata) > 0:
                sewdata['at'] = pd.to_datetime(sewdata['at'])
                sewdata["at"] = sewdata["at"].dt.tz_localize('UCT')
                sewdata["at"] = sewdata["at"].dt.tz_convert('CET')
                sewdata['date'] = pd.to_datetime(sewdata['date'])
                sewdata["date"] = sewdata["date"].dt.tz_localize('UCT')
                sewdata["date"] = sewdata["date"].dt.tz_convert('CET')
                # added
                if not 'position_id' in sewdata.columns:
                    sewdata['position_id'] = sewdata.index
                
                # drop useless columns
                sewdata.drop(axis = 1,
                             columns = ['clr','numberOfAnchors','plan_reference'], inplace=True)

                data = pd.concat([data,sewdata],axis=0, ignore_index=1)

    return data


# combine all functions in a single one  -HOURLY DATA
def read_all_data_hour(startdate,enddate,path):
    """
    Description
    -----------
    Uses the above functions to read and (outer) merge all data of position 
    and accelerometers into a single DataFrame

    Parameters
    ----------
    startdate : TYPE = datetime
        Startdate for data read
    enddate : TYPE = datetime
        Enddate for data read
    path : TYPE = str
        path where directories "db/", "position/" and "history/" are - these
        contain the actual data files to read.

    Returns
    -------
    data : TYPE = pandas DataFrame
        Df with all the data, also the data that could not be merged between 
        position and accelerometer.

    """
    # read feeds in [startdate,enddate]
    feeds,streams,streamfeeds = read_feeds(startdate, enddate, path)

    # read+convert accelerometer data from [startdate,enddate]
    path_acc = os.path.join(path, "history_hour")
    accel = read_parquet_accel_hour(startdate, enddate, path_acc)
    accel["acc_x"] = convert_accel_data(accel["acc_x"])
    accel["acc_y"] = convert_accel_data(accel["acc_y"])
    accel["acc_z"] = convert_accel_data(accel["acc_z"])
    
    # delete measurements with no acceleration; or when acc_x is available but no y,z
    accel = accel.loc[accel["acc_y"].isna() == False,:]

    # read position data
    path_pos = os.path.join(path, "position_hour")
    pos = read_parquet_pos_hour(startdate, enddate, path_pos)
    if len(pos)==0:
        pos = pd.DataFrame([],columns = ["position_id","date","at","feed_reference","X","y"])

    # combine position with feeds
    pos = pos.merge(feeds)
    pos = pos.drop(axis = 1,
                   columns = ["position_id"])

    # combine accel with streams
    accel = accel.merge(streamfeeds,
                        left_on = 'datastream_reference',
                        right_on = 'datastream_id')
    accel = accel.drop(axis = 1,
                       columns = ['history_id','datastream_reference',
                                'datastream_id','d_id','alias'])
    # combine accel with pos using date, at, feed_reference, alias, title
    data = accel.merge(pos,
                       how ='outer',
                       on = ["date","at","feed_reference","title"])

    return data

# function to read accelerometer data of previous two hours
def read_parquet_accel_hour_extra(startdate,path_acc):
    """
    Description
    -----------
    Reads accelerometer data from the parquet files stores in W: drive

    Parameters
    ----------
    startdate : TYPE = datetime
        date of first file you want to read
    enddate : TYPE = datetime
        date of last file you want to read
    path : TYPE = str
        path where files are stored

    Returns
    -------
    data : TYPE = pandas DataFrame
        DataFrame with the read parquet files of acceleration data
        col = ['history_id','date','at','acc_x','acc_y','acc_z']

    """
    # check file path
    if os.path.isdir(path_acc):
        # set filenames based on daterange (.parquet)
        # fn = [f for f in os.listdir(path_acc) if os.path.isfile(os.path.join(path_acc,f)) \
        #         and ".parquet" in f \
        #         and datetime.strptime(f[0:10], '%Y-%m-%d').date() >= startdate \
        #         and datetime.strptime(f[0:10], '%Y-%m-%d').date() <= enddate]
        fn = pd.DataFrame(os.listdir(path_acc),columns = ["fn"])
        fn = fn.sort_values(by = "fn").reset_index(drop=1)
        fn["date"] = pd.to_datetime(fn["fn"].str[0:10], format = '%Y-%m-%d')
        fn["hour"] = pd.to_numeric(fn["fn"].str[11:13])
        fn = fn.loc[(fn["date"] == pd.to_datetime(startdate)) & \
                    (fn["hour"] >= 22) ,:]
        fn = fn["fn"].to_list()

        # read data
        data = pd.DataFrame([])
        for f in range(0,len(fn)):
            # print(fn[f])
            accdata = pd.read_parquet(os.path.join(path_acc,fn[f]), engine='auto')
            if len(accdata) > 0:
                # cast to datetime to reduce computing memory
                accdata['at'] = pd.to_datetime(accdata['at'] )
                accdata["at"] = accdata["at"].dt.tz_localize('UCT')
                accdata["at"] = accdata["at"].dt.tz_convert('CET')
                accdata['date'] = pd.to_datetime(accdata['date'] )
                accdata["date"] = accdata["date"].dt.tz_localize('UCT')
                accdata["date"] = accdata["date"].dt.tz_convert('CET')
                
                # added
                if not 'history_id' in accdata.columns:
                    accdata['history_id'] = accdata.index
    
                # cast to numeric
                accdata['history_id'] = pd.to_numeric(accdata['history_id'],errors='coerce')
    
                #print(fn[f], accdata.memory_usage(deep=True))
    
                # extract values (strings) to temporary dataframe
                value = accdata.value
    
                # drop useless columns
                accdata = accdata.drop(axis = 1,
                                       columns = ['value'])
                value = value.str.split(pat=";",expand=True) # split string + to int
                value = value.rename(columns={0:"acc_x",1:"acc_y",2:"acc_z"}) # rename
    
                # set 'N/A' to 'NaN' for type convertion to float if str
                try:
                    value.loc[value["acc_x"].str.contains('N/A'),"acc_x"] = 'NaN'
                except:
                    pass
                try:
                    value.loc[value["acc_y"].str.contains('N/A'),"acc_y"] = 'NaN'
                except:
                    pass
                try:
                    value.loc[value["acc_z"].str.contains('N/A'),"acc_z"] = 'NaN'
                except:
                    pass
                # drop rows with '%' values in x_acceleration
                value = value.drop(value[(value["acc_x"].str.contains("%")==True)].index)
                
                # drop none in y and z acceleration
                value = value.drop(value[(value["acc_y"].isnull())].index)
                
                # cast to numeric
                value['acc_x'] = pd.to_numeric(value['acc_x'],errors='coerce')
                value['acc_y'] = pd.to_numeric(value['acc_y'],errors='coerce')
                value['acc_z'] = pd.to_numeric(value['acc_z'],errors='coerce')
                accdata['acc_x'] = value['acc_x']
                accdata['acc_y'] = value['acc_y']
                accdata['acc_z'] = value['acc_z']
                del value
    
                data = pd.concat([data,accdata],axis=0, ignore_index=1)
            
            if len(data) > 0:
                data = data.sort_values(by = "at").reset_index(drop=1)
            else:
                data = pd.DataFrame([], columns = ["history_id","date","datastream_reference","at","acc_x","acc_y","acc_z"])
            
    return data

def read_parquet_pos_hour_extra(startdate,path_pos):
    """
    Description
    -----------
    Read position data from sewio db files stored in W: as parquet

    Parameters
    ----------
    startdate : TYPE = datetime
        date of first file you want to read
    enddate : TYPE = datetime
        date of last file you want to read
    path : TYPE = str
        path where files are stored

    Returns
    -------
    data : pandas DataFrame
        pandas data frame containing x,y position.

    """
    # check file path
    if os.path.isdir(path_pos):
        # set filenames based on daterange (.parquet)
        # fn = [f for f in os.listdir(path_pos) if os.path.isfile(os.path.join(path,f)) \
        #         and ".parquet" in f \
        #         and datetime.strptime(f[0:10], '%Y-%m-%d').date() >= startdate \
        #         and datetime.strptime(f[0:10], '%Y-%m-%d').date() <= enddate]
        # fn.sort()
        fn = pd.DataFrame(os.listdir(path_pos),columns = ["fn"])
        fn = fn.sort_values(by = "fn").reset_index(drop=1)
        fn["date"] = pd.to_datetime(fn["fn"].str[0:10], format = '%Y-%m-%d')
        fn["hour"] = pd.to_numeric(fn["fn"].str[11:13])
        fn = fn.loc[(fn["date"] == pd.to_datetime(startdate)) & \
                    (fn["hour"] >= 22) ,:]
        fn = fn["fn"].to_list()
        
        # read data
        data = pd.DataFrame([])
        for f in range(0,len(fn)):
            # print(fn[f])
            sewdata = pd.read_parquet(os.path.join(path_pos,fn[f]))
            if len(sewdata) > 0:
                sewdata['at'] = pd.to_datetime(sewdata['at'])
                sewdata["at"] = sewdata["at"].dt.tz_localize('UCT')
                sewdata["at"] = sewdata["at"].dt.tz_convert('CET')
                sewdata['date'] = pd.to_datetime(sewdata['date'])
                sewdata["date"] = sewdata["date"].dt.tz_localize('UCT')
                sewdata["date"] = sewdata["date"].dt.tz_convert('CET')
                # added
                if not 'position_id' in sewdata.columns:
                    sewdata['position_id'] = sewdata.index
                
                # drop useless columns
                sewdata.drop(axis = 1,
                             columns = ['clr','numberOfAnchors','plan_reference'], inplace=True)

                data = pd.concat([data,sewdata],axis=0, ignore_index=1)
        if len(data) == 0:
            data = pd.DataFrame([], columns = ["position_id","date","at","feed_reference","X","y"])

    return data


# read all data from 2 previous hours of the day
def read_all_data_hour_extra(startdate,path):
    """
    Description
    -----------
    Uses the above functions to read and (outer) merge all data of position 
    and accelerometers into a single DataFrame

    Parameters
    ----------
    startdate : TYPE = datetime
        Startdate for data read
    enddate : TYPE = datetime
        Enddate for data read
    path : TYPE = str
        path where directories "db/", "position/" and "history/" are - these
        contain the actual data files to read.

    Returns
    -------
    data : TYPE = pandas DataFrame
        Df with all the data, also the data that could not be merged between 
        position and accelerometer.

    """
    # read feeds in [startdate,enddate]
    feeds,streams,streamfeeds = read_feeds(startdate, startdate, path)

    # read+convert accelerometer data from [startdate,enddate]
    path_acc = os.path.join(path, "history_hour")
    accel = read_parquet_accel_hour_extra(startdate, path_acc)
    accel["acc_x"] = convert_accel_data(accel["acc_x"])
    accel["acc_y"] = convert_accel_data(accel["acc_y"])
    accel["acc_z"] = convert_accel_data(accel["acc_z"])
    
    # delete measurements with no acceleration; or when acc_x is available but no y,z
    accel = accel.loc[accel["acc_y"].isna() == False,:]

    # read position data
    path_pos = os.path.join(path, "position_hour")
    pos = read_parquet_pos_hour_extra(startdate, path_pos)

    # combine position with feeds
    pos = pos.merge(feeds)
    pos = pos.drop(axis = 1,
                   columns = ["position_id"])

    # combine accel with streams
    accel = accel.merge(streamfeeds,
                        left_on = 'datastream_reference',
                        right_on = 'datastream_id')
    accel = accel.drop(axis = 1,
                       columns = ['history_id','datastream_reference',
                                'datastream_id','d_id','alias'])
    # combine accel with pos using date, at, feed_reference, alias, title
    data = accel.merge(pos,
                       how ='outer',
                       on = ["date","at","feed_reference","title"])

    return data

