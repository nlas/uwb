# NLAS tools

__*created by:*__ Ines Adriaens - adria036  
__*created on:*__ 16/02/2023   
__*collaborators:*__ Bert Klandermans  

### Context

NLAS aims at developing tools for the next generation of animal science. In our case, *"locomotion of dairy cows"*, we developed technology to 
track and monitor locomotion of dairy cows in a continuous and automated way. We worked with 2 different technologies: a UWB-based positioning
system, and video analyses.  

This document lists and explains the tools that are developed in the context of NLAS using the UWB data collected at Dairy Campus, Leeuwarden.
Most tools described below consist of software code to collect, save, quality-control, preprocess and interpret the positioning data.  

### Description of the sensor system and setup

A real-time UWB positioning system was installed at Dairy Campus, Leeuwarden, the Netherlands. It consists of antennas (at least 4 per barn), and tags that
are mounted on the top-side of the neck of the cows. Only when a tag connects to all 4 antennas, the sensor deems to have a reliable signal and the location is 
calculated, with an estimated accuracy of 30 cm. 
The initial installation was done in 2 barns (70 and 72) at end of 2021 and included data collection of 32 cows with the measurment frequency set to 10Hz and
the accelerometers activated. The second part of the intallation in September 2022 entailed 4 additional barns and the waiting area (with 5 antennas).
As the UWB system monitors positions of the tags, the tag id needs to be coupled with the cow id in order to render the information useful.
Battery life of the tag depends on measurement frequency and activation of additional sensors (some have acceleration, gyroscope and magnetometer mounted) and
therefore varies between approx. 14 and 90 days. 
The data are automatically written to a sewio-database that can be accessed via the [IP address](http://10.82.16.215/login) and login information. This 
gives access to the database and real-time visualisation tools, such as demonstrated in the figure below. 

![sensmap visualisation](/documentation/sensmap_visualisation.png) 

From the sewio database, the data are stored on the server system of Dairy Campus, and from there, daily copied to .parquet files (efficient flat file storage) at the 
[W:/WLR_Dataopslag](W:\ASG\WLR_Dataopslag\DairyCampus\3406_Nlas\raw) from Wageningen Livestock Research. Following tables contain the information we need:
- _history_: accelerometer records
- _position_: position records
- _feeds_: feed information, i.e. the tag ids for the position table
- _streamfeeds_: feed information, i.e. the tag ids for the acceleration table  

The cow ids are stored in "Copy_of_sewio_tags.xlsx", as manually entered by the interns and personnel at DC upon changing of tags for battery charging. All
code is written in python, and for each repository, package requirements and versions are available in the "requirement.txt" files.

Usage:
1. Create a new local enviroment, for example via Anaconda with  

`conda create -n <nlas_uwb> python=3.10`

or if you work with a specific IDE

`conda create -n <nlas_uwb> python=3.10 spyder`

Then, activate your environment (`conda activate nlas_uwb`), navigate to the folder in which you cloned the repo with `cd <path>` 
and use pip to install the requirements:
`pip install -r requirements.txt`

Then you can run the codes either via the IDE (spyder) or via the anaconda prompt, or powershell as preferred.

Additional information can be given upon request via <ines.adriaens@kuleuven.be>.

## Tools
-----------------------------------------------------------------------------------------------------------------

**1. Tool to read and combine acceleration/uwb data**

- **Filenames**: read_data_sewio.py || read_data_sewio_LF.py || 
- **Repository**: [UWB quality control](https://git.wur.nl/DairyCampus/NLAS/nlas_datamanagement/data_quality_control_raw/-/tree/main/uwb)
- **Developers**: Ines Adriaens, Bert Klandermans

**_Description_**  

These files contain all the functions needed to read, preprocess and combine both the UWB data and the acceleration data with the cow ids. It translates the
raw activity measures to interpretable numbers based on the dynamic range (individual per sensor, but technically all should be 8, not allways the case, though).
Besides linking the IDS, it also corrects for the time-errors (the sensor systems records on UTF time, causing a difference of 1 or 2 hours
when CET is summer (UTF+1) or winter (UTF+2). These functions are designed to process all files between a preset start and end date, making it
more efficient if only part of the data files / a specific period of interest needs to be processed. In the file with extension "_LF", the functions
are adjusted for computational efficacy, they can deal with the data collected since the extension of the system to all barns and they make use of the data
stored per hour instead of per day, and thus, it is recommended to use when many days need to be processed.

The functions can be called with the script "data_preparation.py" and in the quality control pipeline (see later).



**2. Tool to verify quality control of the uwb data**  

- **Filenames**: qc_parquet_cmd.py || qc_per_day_cmd.py || qc_ping_cowid_cmd.py || mailer.py
- **Repository**: [UWB quality control](https://git.wur.nl/DairyCampus/NLAS/nlas_datamanagement/data_quality_control_raw/-/tree/main/uwb)
- **Developers**: Ines Adriaens, Bert Klandermans

**_Description_** 

Built into the pipelines of Dairy Campus, these scripts produce tables and figures with which the quality of the data of the UWB tags can be verified and visualised. 
The "parquet_cmd" file checks the .parquet raw files without loading them, e.g. the number of lines in each file, and writes the results of the summary to an excel document on the W:/.
The "per_day_cmd" file reads the data of a prespecified number of days, and adds cow ids to them. Next, the raw data are summarized per day: number of records, tags, summary statistics with e.g.
the range and number of NaNs. This is handy for example to verify which accelerometer does not function well or is not calibrated. It also summarizes the gaps in the data, and makes some basic plots.
Upon errors, an e-mail is automatically sent to the people that need to check the data.
Bin contains different previous versions of the files, e.g. for development. The _cmd files are run via the command line, not via an IDE.

**3. Tools to read, condense, preprocess and interpret positions**

- **Filenames**: uwbfunctions.py || preprocessing_all_behaviours.py || exploration.py
- **Repository**: [nlas/uwb](https://git.wur.nl/nlas/uwb)
- **Developers**: Ines Adriaens

**_Description_** 

uwbfunctions contains the necessary functions to preprocess and interpret the uwb data. It uses information from the W:// drive (barn_areas.xlsx) with the barn locations for interpreting the behaviours.
When the script preprocessing_all_behaviours is ran, it produces several files with preprocessed and ready-to-work-on data, among which "allday", "summary", "bouts", "activity". These contain information of all 
cows, starting from the raw .parquet files. The detailed contents and their meaning are listed below.
"Exploration" contains functions to visualise and plot the data (over time). For example, it allows to plot the heatmap with the zones in barn_areas, and the ethograms. This is handy when looking into the order,
amount and distribution of the behaviours over time.
Files not named above are integrated in the named files.

Bouts contains information on individual bouts per behaviour, one line per bout. It is calculated based on 2 thresholds: min_length = minimum duration to call it a “bout” e.g. feeding 
of less than 20 seconds is no feeding bout. Interval = time between two bouts of the same behaviour to be considered as separate bouts. This is dependent on the behaviour. For now, I used the following:

|**Behaviour** |**min_length**|**interval**|
|-----------|:---------:|:---------:|
|Feed |	30s	|5*60s|
|Drink|	5s|	5*60s|
|Cubicle	|30s|	2*60s|
|concentrate|	0s|	5*60s|
|Waiting area|	0s|	20*60s|
|Unknown area (gaps)|	10s|	5*60|

-	cowid
-	barn
-	behaviour (cubicle = all together, cubicle_A,_B and _C is at specific locations: A = at outer wall, B is middle cubicle row, C is row closest to feeding rack
-	date
-	start = seconds of the day this behaviour started
-	end = seconds of the day this behaviour ended
-	btime = begin time (timestamp)
-	etime = end time (timestamp)
-	gap = time between two successive bouts of this specific behaviour in minutes, is empty when first bout of that day
-	len1 = length in minutes, endtime – begintime
-	len2 = length in minutes, endtime – begintime minus gaps (smaller than the threshold interval).

_Summary_ contains one line per behaviour per day, in which the bouts for that behaviour are summarized for that day. 
-	cowid
-	barn
-	behaviour (cubicle = all together, cubicle_A,_B and _C is at specific locations: A = at outer wall, B is middle cubicle row, C is row closest to feeding rack
-	date
-	len1 and len2 are the average duration (with and without small gaps)
-	no_bouts = number of bouts
-	gap_med = median gap between bouts
-	total1 = sum of len1 = total duration of behaviour without gaps (time budgets)
-	total2 = sum of len2 = total duration of behaviour with gaps
-	perc = % of the time that that behaviour is noticed

_Activity_ contains a “summary” of the activity / different views on active/nonactive behaviour of the animals:
-	cowid
-	barn
-	date
-	dist = distance that cows have moved, with minimal distance moved filter (in m)
-	perc_act = percentage of the day that they cow is moving
-	perc_act_nan = percentage of the day that the cow is moving, incl. nan values
-	perc_no_lying = percentage of the day the cow is not in the cubicles or the resting area
-	perc_no_lying_nan = percentage of the day the cow is not in the cubicles incl. nans


**Ethograms**
Black lines are the missing data / values. Colored zones agree with colored blocks. From these graphs, you can e.g. derive the 
![example of ethogram](/documentation/spatial_cow_495_20221001.png)

![example of ethogram](/documentation/spatial_cow_907_20221001.png)
  
  
**Time budgets**
Time budgets for a selected number of days in the beginning of October for one cow - view proportions, missing data, variability per day etc.

![example of time budgets for one cow](/documentation/timebudgets517.png)
  
  
**Group behaviour over time - number of bouts, percentage of time spent, total duration in minutes**
- concentrate
![example of time budgets for one cow](/documentation/corr_allbarns_concentrate.png)

- lying behaviour / in cubicles
![example of time budgets for one cow](/documentation/corr_allbarns_cubicle.png)
  
- feeding behaviour
![example of time budgets for one cow](/documentation/corr_allbarns_feed.png)

    
- drinking behaviour
![example of time budgets for one cow](/documentation/corr_allbarns_drink.png)


These figures can be made (exploration.py) per barn separately as well.